# Finistmart Ecommerce Catalog Application

Implemented with Portlets 3.0 and Web Components technologies

Relies on separate backend fm-service that can be deployed as servlet or start standalone

## Development

## Build

To build for Apache Pluto portal container:

```
mvn package -Ppluto
```

liferay is also supported


## Hot frontend files redeploy

```
mvn -Ppluto exec:exec@fastcopy-app exec:exec@fastcopy-jsp -Dtomcat.webapps.dir==/path/to/tomcat/webapps
```

## Build frontend code manually

you can trigger just bundle build task:
 
```
mvn exec:exec@npm-build 
```

it's possible to override args:

```
mvn exec:exec@npm-build -Dexec.executable="npm.cmd" -Dexec.args="run-script build" -Dexec.workingdir="src/main/webapp/fm-catalog"
```