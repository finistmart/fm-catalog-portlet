import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";



export class FmEditOrderDialog extends SkDialog {

    get saveUrl() {
        return this.getAttribute('save-url');
    }

    get orderId() {
        return this.el.querySelector('#orderId');
    }

    set orderId(orderId) {
        this.setAttribute('order-id', orderId);
    }

    get status() {
        return this.el.querySelector('#orderStatus').selectedOptions[0].value;
    }

    get statusEl() {
        return this.el.querySelector('#orderStatus');
    }

    get form() {
        return this.el.querySelector('#orderForm');
    }

    onconfirm(event) {
        this.form.dispatchEvent(new CustomEvent('formsubmit'));
    };

    onOpen(event) {
        console.log('event', event);
        let row = event.target.parentElement;
        this.copyProps(row);
    }

    copyProps(row) {
        this.orderId = this.orderId_value = row.dataset.orderId;
        let map = {
            'orderData': 'orderData', 'orderNotes': 'orderNotes',
            'buyerIdent': 'buyerIdent', 'created': 'orderCreated',
            'updated': 'orderUpdated', 'status': 'orderStatus'
        };
        for (let fieldName of Object.keys(map)) {
            if (row.dataset[fieldName]) {
                this[map[fieldName]] = row.dataset[fieldName];
            } else {
                this[map[fieldName]] = '';
            }
        }
    }

    onOrderStatusChange(el, value) {
        let selectedOption = null;

        let options = this.statusEl.querySelectorAll('option');
        for (let option of options) {
            if (option.value === value) {
                selectedOption = option;
            }
        }
        if (selectedOption !== null) {
            this.statusEl.selectedIndex = selectedOption.index;
            this.statusEl.value = value;
        }
    }

    open(event) {
        super.open();
        if (! this.openHandler) {
            this.openHandler = this.onOpen.bind(this);

            this.form.addEventListener('formsubmitsuccess', (event) => {
                console.log('saved');
                this.dispatchEvent(new CustomEvent('saved', { detail: { }}));
                this.close();
            });
            this.form.addEventListener('formsubmiterror', (event) => {
                console.error(event);
            });
        }
        this.openHandler(event);

    }
}