
import { FmPortletView } from "../fm-portlet-view.js";
import { DataSourceLocal } from "../node_modules/gridy-grid/src/datasource/data-source-local.js";
import { GridyTable } from "../node_modules/gridy-grid/src/table/gridy-table.js";
import { DateTime } from "../node_modules/dateutils/src/DateTime.js";
import { DateFormat } from "../node_modules/dateutils/src/DateFormat.js";
import { DateLocale } from "../node_modules/dateutils/src/DateLocale.js";


export class FmOrdersView extends FmPortletView {

    set isAdmin(isAdmin) {
        this._isAdmin = String(isAdmin) === "true"
    }

    get isAdmin() {
        return this._isAdmin || false;
    }

    fmtInfoRow(field, value, row) {
        let items = '';
        let html = '';
        try {
            items = JSON.parse(value) || [];
            let item = items[1];
            if (item) {
                if (item.match(/^{/)) {
                    let order = JSON.parse(item)[0];
                    if (order.title) {
                        html += order.title + " " + order.qty + " " + order.price + '<br />';
                    }
                }
            } else {
                return html;
            }
        } catch (e) {
            console.log(e);
            html = value;
        }

        return html;
    }

    fmtDateTime(field, value, row) {
        if (value && Array.isArray(value)) {
            let dt = DateTime.fromDateTime(...value);
            let dateStr = DateFormat.format(dt, "Y-m-d H:i", DateLocale.EN);
            return dateStr;
        } else {
            return value ? value : '';
        }
    }

    onRowClick(event) {
        let dialog = document.querySelector(this.editOrderDialogCfg.dialogSl);
        if (dialog != null) {
            dialog.open(event);
        }
    }

    bindOrdersGrid() {
        let dataSource = new DataSourceLocal();
        dataSource.fields = [
            { title: 'Id', path: '$.id', rowattr: { 'data-order-id': (field, value) => `${value}` } },
            { title: 'Buyer', path: '$.buyerIdent', rowattr: { 'data-buyer-ident': (field, value) => `${value}` } },
            { title: 'Info', path: '$.data', fmt: this.fmtInfoRow.bind(this), html: true, rowattr: { 'data-order-data': (field, value) => `${value}` } },
            { title: 'Notes', path: '$.notes', rowattr: { 'data-order-notes': (field, value) => `${value}` } },
            { title: 'Status', path: '$.status', rowattr: { 'data-status': (field, value) => `${value}` } },
            { title: 'Created', path: '$.created', fmt: this.fmtDateTime.bind(this), rowattr: { 'data-created': (field, value) => `${value}` } },
            { title: 'Updated', path: '$.updated', fmt: this.fmtDateTime.bind(this), rowattr: { 'data-updated': (field, value) => `${value}` } }
        ];
        let grid = document.getElementById(this.ns + 'ordersTable');
        grid.dataSource = dataSource;
        if (! this.rowClickHandler && this.editOrderDialogCfg) {
            this.rowClickHandler = this.onRowClick.bind(this);
            grid.onrowclick = this.rowClickHandler;
        }
        let pager = document.getElementById(this.ns + 'ordersPager');
        pager.dataSource = dataSource;
        let dialog = document.querySelector(this.editOrderDialogCfg.dialogSl);
        if (dialog != null) {
            dialog.addEventListener('saved', (event) => {
                location.href = this.editOrderDialogCfg.redirectUrl;
            });
        }
        if (! window.customElements.get('gridy-table')) {
            window.customElements.define('gridy-table', GridyTable);
        } else {
            grid.connectedCallback();
        }
        dataSource.loadData(this.orders.reverse());
    }

    render() {
        this.bindOrdersGrid();
        this.renderTranslations();
        if (this.isAdmin && this.isAdmin !== "false") {
            this.bindConfigDialog();
        }
    }

    connectedCallback() {
        this.render();
    }
}