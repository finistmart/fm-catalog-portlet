
import { FmPortletView } from "../fm-portlet-view.js";


export class FmCatalogView extends FmPortletView {

    render() {
        this.renderTranslations();
        if (this.isAdmin) {
            this.bindConfigDialog();
        }
    }

    connectedCallback() {
        this.render();
    }
}