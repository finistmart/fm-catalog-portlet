

export class FmProductGrid extends HTMLElement {

    get itemsPerRow() {
        return this.getAttribute('items-per-row') || 4;
    }

    get productData() {
        if (! this._productData) {
            let value = this.getAttribute('product-data');
            this._productData = value ? JSON.parse(value) : {};
        }
        return this._productData;
    }

    get productNoImagePath() {
        return this.getAttribute('product-noimage-path');
    }
    get productLinkTplStr() {
        return this.getAttribute('product-link-tpl-str');
    }

    render() {
        let num = 1;
        let row, itemBox;
        this.innerHTML = '';
        if (this.productData.length > 0) {
            for (let product of this.productData) {
                itemBox = document.createElement('div');
                itemBox.classList.add('sk-box-item');
                //itemBox.style.width = (100 / this.itemsPerRow).toString() + '%';
                itemBox.style.width = '192px';
                let productItem = document.createElement('fm-product-item');
                productItem.setAttribute('product-data', JSON.stringify(product));
                productItem.setAttribute('product-noimage-path', this.productNoImagePath);
                productItem.setAttribute('link-tpl-str', this.productLinkTplStr);
                itemBox.appendChild(productItem);
                this.appendChild(itemBox);
                num++;
            }
        } else {
            console.warn("no products found");
        }
    }

    connectedCallback() {
        if (this.productData) {
            this.render();
        }
    }


}