



export class FmProductItem extends HTMLElement {

    get productData() {
        if (! this._productData) {
            let value = this.getAttribute('product-data');
            this._productData = value ? JSON.parse(value) : {};
        }
        return this._productData;
    }

    get productName() {
        return this.productData.name;
    }

    get productNameItTitle() {
        let name = this.productName;
        if (name.length > 10) {
            name = name.substr(0, 10) + '..';
        }
        return name;
    }

    get productNoImagePath() {
        return this.getAttribute('product-noimage-path');
    }

    get price() {
        let fieldValues = this.productData.fieldValues;
        for (let fieldValue of fieldValues) {
            if (fieldValue.infoField.name === 'price') {
                return fieldValue.value;
            }
        }
        return 0;
    }

    get currency() {
        let fieldValues = this.productData.fieldValues;
        for (let fieldValue of fieldValues) {
            if (fieldValue.infoField.name === 'currency') {
                return fieldValue.value;
            }
        }
        return 'RUB';
    }

    get qty() {
        let el = this.querySelector('input[type=number]');
        if (el !== null) {
            return el.value;
        }
        return 0;
    }

    addToCart(event) {
        let data = this.productData;
        data.title = data.name;
        data.price = this.price;
        data.currency = 'RUR';
        data.qty = this.qty;
        document.dispatchEvent(new CustomEvent('cart:additem', { bubbles: true, detail: { item: data }}));
    }

    fmtLink() {
        let linkTplStr = this.getAttribute('link-tpl-str');

        if (linkTplStr) {
            let itemContentsEl = document.createElement('span');
            itemContentsEl.insertAdjacentHTML('beforeend', linkTplStr);
            return this.renderer.renderMustacheVars(itemContentsEl, { productId: this.productData.id });
        }
    }

    render() {
        this.innerHTML = '';
        this.insertAdjacentHTML('beforeend',
            `
            <div class="fm-product-box">
                <div class="fm-product-title"><h2></h2></div>
                <div class="fm-product-preview"> 
                </div>
                <div class="fm-product-price">
                    ${this.price} <span class="tr.product.currency.${this.currency.toLowerCase()}"></span> 
                </div>
                <div class="fm-product-bpane">
                    <input style="width: 2.5em" type="number" value="1">
                    <button class="fm-product-addtocart">+</button>
                </div>
            </div>
        `);
        let linkStr = this.fmtLink();
        if (linkStr) {
            let title = this.querySelector('.fm-product-title > h2');
            title.innerHTML = linkStr;
            title.querySelector('a').innerHTML = this.productNameItTitle;
        }
        let previewEl = this.querySelector('.fm-product-preview');
        if (previewEl != null && this.productData.image) {
            previewEl.style.backgroundSize = 'contain';
            previewEl.style.backgroundRepeat = 'no-repeat';
            previewEl.style.backgroundPosition = 'center';
            previewEl.style.backgroundImage = "url('data:" + this.productData.image.mimeType + ";base64," + this.productData.image.imageData;
            previewEl.style.width = `192px`;
            previewEl.style.height = `192px`;
            previewEl.innerHTML = `<i></i>`;
        } else {
            previewEl.innerHTML = `<img src="${this.productNoImagePath}" />`;
        }
        let button = this.querySelector('.fm-product-addtocart');
        if (button !== null) {
            this.addToCartHandler = this.addToCart.bind(this);
            button.addEventListener('click', this.addToCartHandler);
            previewEl.addEventListener('click', this.addToCartHandler);
        }
    }

    connectedCallback() {
        this.setAttribute('product-id', this.productData.id);
        this.render();
    }
}