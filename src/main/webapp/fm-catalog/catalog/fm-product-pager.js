

import { GridyPager } from "../node_modules/gridy-grid/src/pager/gridy-pager.js";


export class FmProductPager extends GridyPager {


    connectedCallback() {
        super.connectedCallback();
        this.setAttribute('cur-page-num', this.getAttribute('page-num'));
        if (this.data) {
            this.initPageButtons(null, true);
        }
    }

    insertParam(key, value) {
        key = encodeURIComponent(key);
        value = encodeURIComponent(value);

        // kvp looks like ['key1=value1', 'key2=value2', ...]
        var kvp = document.location.search.substr(1).split('&');
        let i=0;

        for(; i<kvp.length; i++){
            if (kvp[i].startsWith(key + '=')) {
                let pair = kvp[i].split('=');
                pair[1] = value;
                kvp[i] = pair.join('=');
                break;
            }
        }

        if(i >= kvp.length){
            kvp[kvp.length] = [key,value].join('=');
        }

        // can return this or...
        let params = kvp.join('&');

        // reload page with new params
        document.location.search = params;
    }

    onclick(event) {
        if (event.target && event.target.tagName === 'A') {
            this.insertParam('pageNum', event.target.parentElement.dataset.num);
        }
    }
}