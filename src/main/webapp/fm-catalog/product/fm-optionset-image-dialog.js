import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";




export class FmOptionSetImageDialog extends SkDialog {

    get saveUrl() {
        return this.getAttribute('save-url');
    }

    get imageValue() {
        return this.image.getAttribute('value');
    }

    get image() {
        return this.dialogEl.querySelector('#optionsetFile');
    }

    get dialogEl() {
        return this.impl.dialog;
    }

    onconfirm(event) {
        let formData = new FormData();
        formData.append("image", this.image.inputEl.files[0]);
        formData.append("optionSetId", this.optionSetId);
        let request = new XMLHttpRequest();
        request.open('POST', this.saveUrl);
        request.onreadystatechange = (event) => {
            if (event.target.status === 200) {
                this.dispatchEvent(new CustomEvent('saved', { detail: { }}));
                this.close();
            }
        };
        request.send(formData);
    };
}