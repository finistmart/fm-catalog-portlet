import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";




export class FmProductImageDialog extends SkDialog {

    get saveUrl() {
        return this.getAttribute('save-url');
    }

    get productId() {
        return this.getAttribute('product-id');
    }

    get imageValue() {
        return this.image.getAttribute('value');
    }

    get image() {
        return this.dialogEl.querySelector('#file');
    }

    get dialogEl() {
        return this.impl.dialog;
    }

    onconfirm(event) {
        let formData = new FormData();
        formData.append("image", this.image.inputEl.files[0]);
        formData.append("mimeType", this.image.inputEl.files[0].type);
        formData.append("productId", this.productId);
        let request = new XMLHttpRequest();
        request.open('POST', this.saveUrl);
        request.onreadystatechange = (event) => {
            if (event.target.status === 200) {
                this.dispatchEvent(new CustomEvent('saved', { detail: { }}));
                this.close();
            }
        };
        request.send(formData);
    };
}