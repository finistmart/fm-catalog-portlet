import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";




export class FmAddProductDialog extends SkDialog {

    get addUrl() {
        return this.getAttribute('add-url');
    }

    get categoryId() {
        return this.dialogEl.querySelector('#categoryId').selectedOptions[0].value;
    }

    get productName() {
        return this.productNameField.value;
    }

    set productName(value) {
        return this.productNameField.value = value;
    }

    get productNameField() {
        return this.dialogEl.querySelector('#productName');
    }

    get dialogEl() {
        return this.impl.dialog;
    }

    onconfirm(event) {
        if (this.productNameField.validity.valid) {
            let formData = new FormData();
            formData.append("categoryId", this.categoryId);
            formData.append("productName", this.productName);
            let request = new XMLHttpRequest();
            request.open('POST', this.addUrl);
            request.onreadystatechange = (event) => {
                if (event.target.status === 200) {
                    this.dispatchEvent(new CustomEvent('saved', {detail: {}}));
                    this.close();
                }
            };
            request.send(formData);
        }
    };
}