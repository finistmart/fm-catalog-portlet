import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";




export class FmFieldValueDialog extends SkDialog {

    get saveUrl() {
        return this.getAttribute('save-url');
    }

    get productId() {
        return this.getAttribute('product-id');
    }

    get infoFieldId() {
        return this.getAttribute('info-field-id');
    }

    set infoFieldId(infoFieldId) {
        return this.setAttribute('info-field-id', infoFieldId);
    }

    get optionSetId() {
        return this.getAttribute('optionset-id');
    }

    get fieldValueEl() {
        return this.dialogEl.querySelector('#fieldValue');
    }

    get fieldValue() {
        return this.fieldValueEl.value;
    }

    set fieldValue(value) {
        return this.fieldValueEl.value = value;
    }

    get fieldValueWrapEl() {
        return this.dialogEl.querySelector('.field-value-wrap');
    }

    get dialogEl() {
        return this.impl.dialog;
    }

    renderValueEdit() {
        let fieldType = this.fieldType;
        let value = this.fieldValue;
        let parent = this.fieldValueWrapEl;
        parent.removeChild(this.fieldValueEl);
        let el;
        if (fieldType === 'text') {
            el = this.renderer.createEl('textarea');
        } else {
            el = this.renderer.createEl('sk-input');
            el.setAttribute('sk-required', '');
            el.innerHTML = 'Value';
        }
        el.value = value;
        el.setAttribute('id', 'fieldValue');
        el.setAttribute('name', 'fieldValue');
        parent.appendChild(el);
    }

    open() {
        super.open();
        this.renderValueEdit();
    }

    onconfirm(event) {
        if (this.fieldValueEl.validity.valid) {
            let formData = new FormData();
            formData.append("fieldValue", this.fieldValue);
            formData.append("infoField", this.infoField);
            formData.append("optionSetId", this.optionSetId);
            formData.append("productId", this.productId);
            formData.append("valueId", this.valueId);
            let request = new XMLHttpRequest();
            request.open('POST', this.saveUrl);
            request.onreadystatechange = (event) => {
                if (event.target.status === 200) {
                    this.dispatchEvent(new CustomEvent('saved', {
                        detail: {
                            fieldValue: this.fieldValue,
                            infoField: this.infoField,
                            infoFieldId: this.infoFieldId,
                            optionSetId: this.optionSetId,
                            productId: this.productId,
                            valueId: this.valueId
                        }
                    }));
                    this.close();
                }
            };
            request.send(formData);
        }
    };
}