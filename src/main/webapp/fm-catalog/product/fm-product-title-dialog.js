

import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";


export class FmProductTitleDialog extends SkDialog {

    get saveUrl() {
        return this.getAttribute('save-url');
    }

    get productId() {
        return this.getAttribute('product-id');
    }

    get optionSetId() {
        return this.getAttribute('optionset-id');
    }

    get productName() {
        return this.productNameField.value;
    }

    set productName(value) {
        return this.productNameField.value = value;
    }

    get productNameField() {
        return this.dialogEl.querySelector('#productName');
    }

    get dialogEl() {
        return this.impl.dialog;
    }

    onconfirm(event) {
        if (this.productNameField.validity.valid) {
            let formData = new FormData();
            formData.append("name", this.productName);
            formData.append("productId", this.productId);
            formData.append("optionSetId", this.optionSetId);
            let request = new XMLHttpRequest();
            request.open('POST', this.saveUrl);
            request.onreadystatechange = (event) => {
                if (event.target.status === 200) {
                    this.dispatchEvent(new CustomEvent('saved', {detail: {value: this.titleValue}}));
                    this.close();
                }
            };
            request.send(formData);
        }
    };
}