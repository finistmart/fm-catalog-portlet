
import { FmPortletView } from "../fm-portlet-view.js";


export class FmProductView extends FmPortletView {

    bindCart() {
        let addToCartBtn = document.querySelector(this.cartBtnSl);
        if (addToCartBtn) {
            let price = this.priceValue;
            addToCartBtn.addEventListener('click', (event) => {
                let item = {title: this.product.name, price: price, currency: 'RUR'};
                document.dispatchEvent(new CustomEvent('cart:additem', {bubbles: true, detail: {item: item}}));
            });
        }
    }

    nl2br(str){
        return str.replace(/(?:\r\n|\r|\n)/g, '<br />');
    }

    bindFieldValueDialog() {
        if (this.fieldValueDialogCfg) {
            let fieldValueEditButtons = document.querySelectorAll(this.fieldValueDialogCfg.buttonsSl);
            let fieldValueDialog = document.querySelector(this.fieldValueDialogCfg.dialogSl);
            for (let fieldValueEditButton of fieldValueEditButtons) {
                if (fieldValueEditButton) {
                    fieldValueEditButton.addEventListener('click', (event) => {
                        let data = event.target ? event.target.dataset : {};
                        fieldValueDialog.infoField = data.infofield;
                        fieldValueDialog.infoFieldId = data.infoFieldId;
                        fieldValueDialog.fieldValue = data.fieldvalue;
                        fieldValueDialog.fieldType = data.fieldType;
                        fieldValueDialog.valueId = data.valueid;
                        fieldValueDialog.open();
                    });
                }
            }
            fieldValueDialog.addEventListener('saved', (event) => {
                let valueEl = document.querySelector('#' + this.ns + 'productPropValue_' + event.detail.valueId);
                if (valueEl) {
                    valueEl.innerHTML = this.nl2br(event.detail.fieldValue);
                }
            });
        }
    }


    bindAddFieldValueDialog() {
        if (this.addFieldValueDialogCfg) {
            let fieldValueAddButton = document.querySelector(this.addFieldValueDialogCfg.buttonSl);
            let fieldValueAddDialog = document.querySelector(this.addFieldValueDialogCfg.dialogSl);
            if (fieldValueAddButton) {
                fieldValueAddButton.addEventListener('click', (event) => {
                    fieldValueAddDialog.open();
                });
                fieldValueAddDialog.addEventListener('saved', (event) => {
                    location.href = this.addFieldValueDialogCfg.redirectUrl;
                });
            }
        }
    }

    bindDelFieldValue() {
        if (this.delFieldValueCfg) {
            let fieldValueAddButtons = document.querySelectorAll(this.delFieldValueCfg.buttonsSl);
            let deleteUrl = this.delFieldValueCfg.deleteUrl;
            let redirectUrl = this.delFieldValueCfg.redirectUrl;
            for (let button of fieldValueAddButtons) {
                button.addEventListener('click', (event) => {
                    let data = event.target.dataset;
                    let formData = new FormData();
                    formData.append("optionSetId", data.optionSetId);
                    formData.append("valueId", data.valueId);
                    formData.append("infoFieldId", data.infoFieldId);
                    let request = new XMLHttpRequest();
                    request.open('POST', deleteUrl);
                    request.onreadystatechange = (event) => {
                        if (event.target.status === 200) {
                            location.href = redirectUrl;
                        }
                    };
                    request.send(formData);
                });

            }
        }
    }

    bindTitleDialog() {
        if (this.titleDialogCfg) {
            let titleEditButton = document.querySelector(this.titleDialogCfg.editButtonSl);
            let titleDialog = document.querySelector(this.titleDialogCfg.dialogSl);
            let productName = document.querySelector(this.titleDialogCfg.productNameSl);
            if (titleEditButton) {
                titleEditButton.addEventListener('click', (event) => {
                    titleDialog.productName = titleDialog.getAttribute('product-title');
                    titleDialog.open();
                });
                titleDialog.addEventListener('saved', (event) => {
                    location.href = this.titleDialogCfg.redirectUrl;
                })
            }
        }
    }

    bindImageDialog() {
        if (this.imageDialogCfg) {
            let imageEditButton = document.querySelector(this.imageDialogCfg.buttonSl);
            let imageDialog = document.querySelector(this.imageDialogCfg.dialogSl);
            if (imageEditButton) {
                imageEditButton.addEventListener('click', (event) => {
                    imageDialog.open();
                });
                imageDialog.addEventListener('saved', (event) => {
                    location.href = this.imageDialogCfg.redirectUrl;
                });
            }
        }
    }

    bindOptImageDialog() {
        if (this.optImageDialogCfg) {
            let optImageEditButtons = document.querySelectorAll(this.optImageDialogCfg.buttonSl);
            let optImageDialog = document.querySelector(this.optImageDialogCfg.dialogSl);
            for (let optImageEditButton of optImageEditButtons) {
                if (optImageEditButton) {
                    optImageEditButton.addEventListener('click', (event) => {
                        optImageDialog.optionSetId = event.target.dataset.optionsetid;
                        optImageDialog.open();
                    });
                }
            }
            optImageDialog.addEventListener('saved', (event) => {
                location.href = this.optImageDialogCfg.redirectUrl;
            });
        }
    }

    bindAddOptionDialog() {
        if (this.addOptionDialogCfg) {
            let optAddButton = document.querySelector(this.addOptionDialogCfg.buttonSl);
            let optAddDialog = document.querySelector(this.addOptionDialogCfg.dialogSl);
            if (optAddButton) {
                optAddButton.addEventListener('click', (event) => {
                    optAddDialog.open();
                });
                optAddDialog.addEventListener('saved', (event) => {
                    location.href = this.addOptionDialogCfg.redirectUrl;
                });
            }
        }
    }

    bindAddProductDialog() {
        if (this.addProductDialogCfg) {
            let productAddButton = document.querySelector(this.addProductDialogCfg.buttonSl);
            let addProductDialog = document.querySelector(this.addProductDialogCfg.dialogSl);
            if (productAddButton) {
                productAddButton.addEventListener('click', (event) => {
                    addProductDialog.open();
                });
                productAddButton.addEventListener('saved', (event) => {
                    location.href = this.addFieldValueDialogCfg.redirectUrl;
                });
            }
        }
    }

    bindSetCategoryDialog() {
        if (this.setCategoryDialogCfg) {
            let setCategoryButton = document.querySelector(this.setCategoryDialogCfg.buttonSl);
            let setCategoryDialog = document.querySelector(this.setCategoryDialogCfg.dialogSl);
            if (setCategoryButton) {
                setCategoryButton.addEventListener('click', (event) => {
                    setCategoryDialog.open();
                });
                setCategoryButton.addEventListener('saved', (event) => {
                    location.href = this.setCategoryDialogCfg.redirectUrl;
                });
            }
        }
    }

    deleteProduct() {
        let deleteUrl = this.deleteProductCfg.deleteUrl;
        let redirectUrl = this.deleteProductCfg.redirectUrl;
        let productId = this.deleteProductCfg.productId;
        let optionSetId = this.deleteProductCfg.optionSetId;
        let formData = new FormData();
        formData.append("productId", productId);
        formData.append("optionSetId", optionSetId);
        let request = new XMLHttpRequest();
        request.open('POST', deleteUrl);
        request.onreadystatechange = (event) => {
            if (event.target.status === 200) {
                location.href = redirectUrl;
            }
        };
        request.send(formData);
    }

    bindDeleteProduct() {
        if (this.deleteProductCfg) {
            let button = document.querySelector(this.deleteProductCfg.buttonSl);
            if (button) {
                if (! this.deleteHandler) {
                    this.deleteHandler = this.deleteProduct.bind(this);
                    button.addEventListener('click', this.deleteHandler);
                }
            }
        }
    }


    set isAdmin(isAdmin) {
        this._isAdmin = String(isAdmin) === "true"
    }

    get isAdmin() {
        return this._isAdmin || false;
    }

    render() {
        this.bindCart();
        this.renderTranslations();
        if (this.isAdmin && this.isAdmin !== "false") {
            this.bindConfigDialog();
            this.bindFieldValueDialog();
            this.bindTitleDialog();
            this.bindImageDialog();
            this.bindOptImageDialog();
            this.bindAddOptionDialog();
            this.bindAddFieldValueDialog();
            this.bindDelFieldValue();
            this.bindAddProductDialog();
            this.bindDeleteProduct();
            this.bindSetCategoryDialog();
        }
    }

    connectedCallback() {
        this.render();
    }
}