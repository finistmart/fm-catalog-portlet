import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";



export class FmSetCategoryDialog extends SkDialog {

    get saveUrl() {
        return this.getAttribute('save-url');
    }

    get productId() {
        return this.getAttribute('product-id');
    }

    get categoryId() {
        return this.dialogEl.querySelector('#categoryId').selectedOptions[0].value;
    }

    get curCategoryId() {
        return this.getAttribute('cur-category-id');
    }

    get dialogEl() {
        return this.impl.dialog;
    }

    onconfirm(event) {
        let formData = new FormData();
        formData.append("categoryId", this.categoryId);
        formData.append("productId", this.productId);
        let request = new XMLHttpRequest();
        request.open('POST', this.saveUrl);
        request.onreadystatechange = (event) => {
            if (event.target.status === 200) {
                this.dispatchEvent(new CustomEvent('saved', { detail: { }}));
                this.close();
            }
        };
        request.send(formData);
    };


    onOpen() {
        if (this.curCategoryId) {
            let selectedOption = null;
            let options = this.dialogEl.querySelector('#categoryId').querySelectorAll('option');
            for (let option of options) {
                if (option.value === this.curCategoryId) {
                    selectedOption = option;
                }
            }
            if (selectedOption !== null) {
                this.dialogEl.querySelector('#categoryId').selectedIndex = selectedOption.index;
            }
        }
    }

    open() {
        if (! this.openHandler) {
            this.openHandler = this.onOpen.bind(this);
            this.openHandler();
        }
        super.open();
    }
}