import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";




export class FmAddOptionSetDialog extends SkDialog {

    get addUrl() {
        return this.getAttribute('add-url');
    }

    get productId() {
        return this.getAttribute('product-id');
    }

    get optionId() {
        return this.dialogEl.querySelector('#optionSelect').value;
    }

    get nameValue() {
        return this.nameField.value;
    }

    set nameValue(value) {
        return this.nameField.value = value;
    }

    get nameField() {
        return this.dialogEl.querySelector('#nameInput');
    }

    get dialogEl() {
        return this.impl.dialog;
    }

    get btnCancelEl() {
        return this.dialogEl.querySelector('.btn-cancel');
    }

    get btnConfirmEl() {
        return this.dialogEl.querySelector('.btn-confirm');
    }

    get closeBtn() {
        return this.dialogEl.querySelector('.ant-modal-close');
    }

    onconfirm(event) {
        if (this.nameField.validity.valid) {
            let formData = new FormData();
            formData.append("name", this.nameValue);
            formData.append("productId", this.productId);
            formData.append("optionId", this.optionId);
            let request = new XMLHttpRequest();
            request.open('POST', this.addUrl);
            request.onreadystatechange = (event) => {
                if (event.target.status === 200) {
                    this.dispatchEvent(new CustomEvent('saved', {detail: {value: this.nameValue}}));
                    this.close();
                }
            };
            request.send(formData);
        }
    };
}