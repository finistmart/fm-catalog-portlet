import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";




export class FmAddFieldValueDialog extends SkDialog {

    get addUrl() {
        return this.getAttribute('add-url');
    }

    get productId() {
        return this.getAttribute('product-id');
    }

    get optionSetId() {
        return this.getAttribute('optionset-id');
    }

    get infoFieldId() {
        return this.dialogEl.querySelector('#infoFieldId').selectedOptions[0].value;
    }

    get fieldValueEl() {
        return this.dialogEl.querySelector('#fieldValue');
    }

    get fieldValue() {
        return this.fieldValueEl.value;
    }

    set fieldValue(value) {
        return this.fieldValueEl.value = value;
    }

    get fieldValueWrapEl() {
        return this.dialogEl.querySelector('.field-value-wrap');
    }

    get dialogEl() {
        return this.impl.dialog;
    }

    onOptionChange(event) {
        if (this.fieldValueEl.validity.valid) {
            let selected = this.dialogEl.querySelector('#infoFieldId').selectedOptions[0];
            if (selected != null) {
                let fieldType = selected.dataset.fieldType;
                let parent = this.fieldValueWrapEl;
                parent.removeChild(this.fieldValueEl);
                let el;
                if (fieldType === 'text') {
                    el = this.renderer.createEl('textarea');
                } else {
                    el = this.renderer.createEl('sk-input');
                    el.innerHTML = 'Value';
                    el.setAttribute('sk-required', '');
                }
                el.setAttribute('id', 'fieldValue');
                el.setAttribute('name', 'fieldValue');
                parent.appendChild(el);
            }
        }
    }

    onOpen() {
        if (! this.optionChangeHandler) {
            this.optionChangeHandler = this.onOptionChange.bind(this);
            this.dialogEl.querySelector('#infoFieldId').addEventListener('change', this.optionChangeHandler);
            this.optionChangeHandler();
        }
    }

    open() {
        super.open();
        if (! this.openHandler) {
            this.openHandler = this.onOpen.bind(this);
            this.onOpen();
        }
    }

    onconfirm(event) {
        let formData = new FormData();
        formData.append("fieldValue", this.fieldValue);
        formData.append("productId", this.productId);
        formData.append("optionSetId", this.optionSetId);
        formData.append("infoFieldId", this.infoFieldId);
        let request = new XMLHttpRequest();
        request.open('POST', this.addUrl);
        request.onreadystatechange = (event) => {
            if (event.target.status === 200) {
                this.dispatchEvent(new CustomEvent('saved', { detail: { value: this.fieldValue }}));
                this.close();
            }
        };
        request.send(formData);
    };
}