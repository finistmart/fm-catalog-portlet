import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";



export class FmEditCategoryDialog extends SkDialog {

    get saveUrl() {
        return this.getAttribute('save-url');
    }

    get editCategoryId() {
        return this.dialogEl.querySelector('#editCategoryId').selectedOptions[0].value;
    }

    get parentCategoryId() {
        return this.dialogEl.querySelector('#parentCategoryId').selectedOptions[0].value;
    }

    get nameValue() {
        return this.nameField.getAttribute('value');
    }

    set nameValue(value) {
        return this.nameField.setAttribute('value', value);
    }

    get nameField() {
        return this.dialogEl.querySelector('#nameInput');
    }

    get dialogEl() {
        return this.impl.dialog;
    }

    onconfirm(event) {
        if (this.nameField.validity.valid) {
            let formData = new FormData();
            formData.append("name", this.nameValue);
            formData.append("editCategoryId", this.editCategoryId);
            formData.append("parentCategoryId", this.parentCategoryId);

            let request = new XMLHttpRequest();
            request.open('POST', this.saveUrl);
            request.onreadystatechange = (event) => {
                if (event.target.status === 200) {
                    this.dispatchEvent(new CustomEvent('saved', {detail: {value: this.nameValue}}));
                    this.close();
                }
            };
            request.send(formData);
        }
    };

    onOpen() {
        this.dialogEl.querySelector('#editCategoryId').addEventListener('change', (event) => {
            if (event.target && event.target.selectedOptions) {
                let parentId = event.target.selectedOptions[0].dataset.parentId;
                let selectedOption = null;
                let options = this.dialogEl.querySelector('#parentCategoryId').querySelectorAll('option');
                for (let option of options) {
                    if (option.value === parentId) {
                        selectedOption = option;
                    }
                }
                if (selectedOption !== null) {
                    this.dialogEl.querySelector('#parentCategoryId').selectedIndex = selectedOption.index;
                }
            }
        });
    }

    open() {
        if (! this.openHandler) {
            this.openHandler = this.onOpen.bind(this);
            this.openHandler();
        }
        super.open();
    }
}