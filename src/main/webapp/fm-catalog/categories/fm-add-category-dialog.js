import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";




export class FmAddCategoryDialog extends SkDialog {

    get addUrl() {
        return this.getAttribute('add-url');
    }

    get parentCategoryId() {
        return this.dialogEl.querySelector('#parentCategoryId').selectedOptions[0].value;
    }

    get nameValue() {
        return this.nameField.getAttribute('value');
    }

    set nameValue(value) {
        return this.nameField.setAttribute('value', value);
    }

    get nameField() {
        return this.dialogEl.querySelector('#nameInput');
    }

    get dialogEl() {
        return this.impl.dialog;
    }

    onconfirm(event) {
        if (this.nameField.validity.valid) {
            let formData = new FormData();
            formData.append("name", this.nameValue);

            formData.append("parentCategoryId", this.parentCategoryId);

            let request = new XMLHttpRequest();
            request.open('POST', this.addUrl);
            request.onreadystatechange = (event) => {
                if (event.target.status === 200) {
                    this.dispatchEvent(new CustomEvent('saved', {detail: {value: this.nameValue}}));
                    this.close();
                }
            };
            request.send(formData);
        }
    };
}