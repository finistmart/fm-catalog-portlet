
import { FmPortletView } from "../fm-portlet-view.js";


export class FmCategoriesView extends FmPortletView {


    bindAddCategoryDialog() {
        if (this.addCategoryDialogCfg) {
            let addCategoryBtn = document.querySelector(this.addCategoryDialogCfg.buttonSl);
            let addCategoryDialog = document.querySelector(this.addCategoryDialogCfg.dialogSl);
            if (addCategoryBtn) {
                addCategoryBtn.addEventListener('click', (event) => {
                    let data = event.target ? event.target.dataset : {};
                    //addCategoryDialog.infoField = data.infofield;
                    addCategoryDialog.open();
                });
            }
            addCategoryDialog.addEventListener('saved', (event) => {
                location.href = this.addCategoryDialogCfg.redirectUrl;
            });
        }
    }

    bindEditCategoryDialog() {
        if (this.editCategoryDialogCfg) {
            let editategoryBtn = document.querySelector(this.editCategoryDialogCfg.buttonSl);
            let editCategoryDialog = document.querySelector(this.editCategoryDialogCfg.dialogSl);
            if (editategoryBtn) {
                editategoryBtn.addEventListener('click', (event) => {
                    let data = event.target ? event.target.dataset : {};
                    //addCategoryDialog.infoField = data.infofield;
                    editCategoryDialog.open();
                });
            }
            editCategoryDialog.addEventListener('saved', (event) => {
                location.href = this.editCategoryDialogCfg.redirectUrl;
            });
        }
    }

    bindDeleteCategoryDialog() {
        if (this.deleteCategoryDialogCfg) {
            let deleteCategoryBtn = document.querySelector(this.deleteCategoryDialogCfg.buttonSl);
            let deleteCategoryDialog = document.querySelector(this.deleteCategoryDialogCfg.dialogSl);
            if (deleteCategoryBtn) {
                deleteCategoryBtn.addEventListener('click', (event) => {
                    let data = event.target ? event.target.dataset : {};
                    //addCategoryDialog.infoField = data.infofield;
                    deleteCategoryDialog.open();
                });
            }
            deleteCategoryDialog.addEventListener('saved', (event) => {
                location.href = this.deleteCategoryDialogCfg.redirectUrl;
            });
        }
    }

    bindCategoriesNavigation() {
        if (this.categoriesCfg) {
            let categoryMobileSelect = document.querySelector(this.categoriesCfg.categorySelectMob);
            if (categoryMobileSelect) {
                categoryMobileSelect.addEventListener('change', (event) => {
                   let categoryId = event.target.value;
                   if (categoryId && categoryId !== '0') {
                       location.href = '/?categoryId=' + categoryId;
                   } else {
                       location.href = '/';
                   }
                });
            }
        }
    }

    set isAdmin(isAdmin) {
        this._isAdmin = String(isAdmin) === "true"
    }

    get isAdmin() {
        return this._isAdmin || false;
    }

    render() {
        this.bindCategoriesNavigation();
        this.renderTranslations();
        if (this.isAdmin && this.isAdmin !== "false") {
            this.bindConfigDialog();
            this.bindAddCategoryDialog();
            this.bindEditCategoryDialog();
            this.bindDeleteCategoryDialog();
        }
    }

    connectedCallback() {
        this.render();
    }
}