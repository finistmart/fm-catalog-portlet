import { SkDialog } from "../node_modules/skinny-widgets/src/sk-dialog.js";



export class FmDeleteCategoryDialog extends SkDialog {

    get deleteUrl() {
        return this.getAttribute('delete-url');
    }

    get categoryId() {
        return this.el.querySelector('#categoryId').selectedOptions[0].value;
    }



    onconfirm(event) {
        let formData = new FormData();
        formData.append("name", this.nameValue);
        formData.append("categoryId", this.categoryId);

        let request = new XMLHttpRequest();
        request.open('POST', this.deleteUrl);
        request.onreadystatechange = (event) => {
            if (event.target.status === 200) {
                this.dispatchEvent(new CustomEvent('saved', { detail: {  }}));
                this.close();
            }
        };
        request.send(formData);
    };
}