<%@ page import="java.util.Set" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.List" %>
<%@ page import="javax.portlet.RenderURL" %>
<%@ page import="com.finistmart.catalog.portlet.dto.Product" %>
<%@ page import="com.finistmart.catalog.portlet.dto.ProductOptionSet" %>
<%@ page import="com.finistmart.catalog.portlet.dto.ProductOption" %>
<%@ page import="com.finistmart.catalog.portlet.dto.InfoField" %>
<%@ page import="com.finistmart.catalog.portlet.dto.Category" %>
<%@ page import="com.finistmart.catalog.portlet.dto.IFieldValue" %>
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<%@ taglib uri="http://xmlns.jcp.org/portlet_3_0"  prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<portlet:defineObjects />

<% if (renderRequest.isUserInRole("administrator")) { %>
<portlet:actionURL var="saveSettingsURL" name="saveSettings"></portlet:actionURL>
<portlet:actionURL var="saveNameURL" name="saveName"></portlet:actionURL>
<portlet:actionURL var="saveImageURL" name="saveImage"></portlet:actionURL>
<portlet:actionURL var="saveOptImageURL" name="saveOptionSetImage"></portlet:actionURL>
<portlet:actionURL var="saveFieldValueURL" name="saveFieldValue"></portlet:actionURL>
<portlet:actionURL var="addFieldValueURL" name="addFieldValue"></portlet:actionURL>
<portlet:actionURL var="delFieldValueURL" name="delFieldValue"></portlet:actionURL>
<portlet:actionURL var="addOptionSetURL" name="addOptionSet"></portlet:actionURL>
<portlet:actionURL var="addProductURL" name="addProduct"></portlet:actionURL>
<portlet:actionURL var="setCategoryURL" name="setCategory"></portlet:actionURL>
<portlet:actionURL var="deleteOptionSetOrProductURL" name="deleteOptionSetOrProduct"></portlet:actionURL>
<portlet:resourceURL var="getSettingsURL" id="getSettings"></portlet:resourceURL>
<portlet:resourceURL var="getOptionSetLinkURL" id="getOptionSetLink"></portlet:resourceURL>
<portlet:renderURL var="renderURL"></portlet:renderURL>
<portlet:actionURL var="clearCacheURL" name="clearCache"></portlet:actionURL>
<% } %>

<portlet:renderURL var="optionURL">
</portlet:renderURL>
<%
    String ctxPath = request.getContextPath();
    List<String> settings = (List<String>) renderRequest.getAttribute("settings");
    Product product = (Product) renderRequest.getAttribute("product");
    String currency = (String) renderRequest.getAttribute("currency");
    Map<String, IFieldValue> fieldValues = (Map<String, IFieldValue>) renderRequest.getAttribute("totalFieldValueMap");
    List<ProductOption> productOptions = (List<ProductOption> ) renderRequest.getAttribute("productOptions");
    List<InfoField> unsetInfoFields = (List<InfoField>) renderRequest.getAttribute("unsetInfoFields");
    List<Category> categories = (List<Category>) renderRequest.getAttribute("categories");
%>
<style>
    .fm-btn-configure {
        background-image: url('<%= ctxPath %>/images/settings_black_192x192.png');
        background-size: contain;
        width: 50px;
        height: 50px;
        float: right;
        margin-top: -50px;
    }
    .ant-modal-body {
        min-width: 400px;
    }
    .ant-modal-body textarea {
        min-width: 350px;
        min-height: 100px;
    }
</style>



<sk-config
        theme="antd"
        base-path="<%= request.getContextPath()%>/fm-catalog/node_modules/skinny-widgets/src"
></sk-config>

<% if (renderRequest.isUserInRole("administrator")) { %>

<product-config-dialog
        id="${renderResponse.getNamespace()}configDialog"
        ns="${renderResponse.getNamespace()}"
        saveUrl="${saveSettingsURL.toString()}"
        settingsUrl="${getSettingsURL.toString()}"
        type="confirm" to-body
>
    <h3>Settings</h3>
    <div>
        <% for (String settingName : settings) { %>
            <div>
                <label><%= settingName %></label>
                <sk-input type="text" name="<%= settingName %>" value="<%= (String) renderRequest.getAttribute(settingName) %>"></sk-input>
            </div>
        <% } %>
    </div>
    <template id="FmProductConfigDialogFooterTpl">
        <div>
            <sk-button action="clearCache">Clear Cache</sk-button>
            <sk-button action="cancel">Cancel</sk-button>
            <sk-button action="save" button-type="primary">Save</sk-button>
        </div>
    </template>

</product-config-dialog>

<button class="fm-btn-configure" id="${renderResponse.getNamespace()}configDialogOpenBtn"></button>

<% } %>


<fm-product-view>

<style>
    .product-columns {
        display: flex;
        flex-direction: column;
    }
    .product-rows {
        display: flex;
        flex-direction: row;
    }
    .fm-option-ava {
        width: 40px;
        height: 40px;
        background-color: gray;
        margin: 10px;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center;
    }
    .fm-option-label {
        text-align: center;
    }
    .fm-productinfo {
        flex-grow: 2;
    }
    .fm-productinfo-contents {
        margin-left: 10px;
    }
    .product-fields {
        font-size: 1.2em;
        margin-top: 20px;
    }
    .fm-product-prop-title, .fm-product-prop-value {
        padding: 0.4em;
        min-width: 150px;
    }
    .fm-product-field-price-wrap {
        font-size: 1.4em;
        color: red;
    }
    .fm-buy-row {
        text-align: right;
    }
    dialog {
        border-width: initial;
        border-style: solid;
        border-color: initial;
        border-image: initial;
        padding: 1em;
        background: white;
    }
    dialog label {
        min-width: 250px;
    }
</style>
<div class="product-columns">
    <div>
        <div class="product-rows">
            <div class="fm-productinfo-preview" style="width: 150px; height: 200px; background-color: gray; background-size: contain; background-image: url('data:${product.image.mimeType};base64,${product.image.imageData}');">
                <% if (renderRequest.isUserInRole("administrator")) { %>
                    <sk-button id="titleEditBtn" class="tr.btn.edit image-btn-edit">edit</sk-button>
                <% } %>
            </div>
            <div class="fm-productinfo">
                <% if (renderRequest.isUserInRole("administrator")) { %>
                    <sk-button id="addProductBtn" class="tr.btn.add product-btn-add">add</sk-button>
                    <sk-button id="deleteProductBtn" class="tr.btn.delete product-btn-delete">delete</sk-button>
                <% } %>
                <div class="product-columns fm-productinfo-contents">
                    <div>
                        <h2 id="<portlet:namespace/>productName">${productTitle}</h2>
                        <% if (renderRequest.isUserInRole("administrator")) { %>
                            <sk-button class="tr.btn.edit title-btn-edit">edit</sk-button>
                        <% } %>
                    </div>
                    <div>
                        <div class="product-options product-rows">
                            <%
                                for (ProductOptionSet optionSet : product.getOptionSets()) {
                                    RenderURL url = renderResponse.createRenderURL();
                                    url.getRenderParameters().setValue("productId", String.valueOf(product.getId()));
                                    url.getRenderParameters().setValue("optionSetId", String.valueOf(optionSet.getId())); %>
                                <div>
                                    <a href="<%= url.toString() %>" class="fm-optionset-link">
                                        <% if (optionSet.getImage() != null) { %>
                                            <div class="fm-option-ava"
                                                 style="background-image: url('data:<%= optionSet.getImage().getMimeType()%>;base64,<%= optionSet.getImage().getImageData()%>');">
                                            </div>
                                        <% } else { %>
                                            <div class="fm-option-ava"></div>
                                        <% } %>
                                        <div class="fm-option-label" title="<%= optionSet.getName() %>" >
                                            <%= optionSet.getName().length() > 6 ? String.valueOf(optionSet.getName()).substring(0, 5) + ".." : optionSet.getName() %>
                                        </div>
                                    </a>
                                    <% if (renderRequest.isUserInRole("administrator")) { %>
                                        <sk-button class="tr.btn.edit optionset-ava-btn-edit" data-optionsetid="<%= String.valueOf(optionSet.getId()) %>">edit</sk-button>
                                    <% } %>
                                </div>
                            <% } %>
                            <% if (renderRequest.isUserInRole("administrator")) { %>
                                <sk-button class="tr.btn.add optionset-btn-add">add</sk-button>
                            <% } %>
                        </div>
                    </div>
                    <div class="product-fields product-columns">
                        <%
                            for (String fieldValueName : fieldValues.keySet()) {
                                IFieldValue fieldValue = fieldValues.get(fieldValueName);
                            %>
                            <div class="product-rows fm-product-prop fm-product-field-<%= fieldValue.getInfoField().getName() %>-wrap" data-fieldvalueid="<%= fieldValue.getId() %>">
                                <div class="tr.product.field.<%= fieldValue.getInfoField().getName() %> fm-product-prop-title">
                                    <%= fieldValue.getInfoField().getName() %>
                                </div>
                                <div class="fm-product-prop-value" id="<portlet:namespace/>productPropValue_<%= fieldValue.getId() %>">
                                    <%=  fieldValue.getValue().replaceAll("(\r\n|\n)", "<br />") %>
                                    <%= "price".equals(fieldValue.getInfoField().getName()) ? currency : "" %>
                                </div>
                                <% if (renderRequest.isUserInRole("administrator")) { %>
                                    <sk-button class="tr.btn.edit prop-btn-edit"
                                       data-info-field-id="<%= fieldValue.getInfoField().getId() %>"
                                       data-infofield="<%= fieldValue.getInfoField().getName() %>"
                                       data-field-type="<%= fieldValue.getInfoField().getFieldType() %>"
                                       data-valueid="<%= String.valueOf(fieldValue.getId())  %>"
                                       data-fieldvalue="<%= fieldValue.getValue() %>">Edit</sk-button>
                                    <sk-button class="tr.btn.delete prop-btn-delete"
                                       data-info-field-id="<%= fieldValue.getInfoField().getId() %>"
                                       data-value-id="<%= fieldValue.getId() %>"
                                       data-option-set-id="${optionSetId}"
                                       >Del</sk-button>
                                <% } %>
                            </div>
                        <% } %>
                        <% if (renderRequest.isUserInRole("administrator")) { %>
                            <sk-button class="tr.btn.add fieldvalue-btn-add">add</sk-button>
                            <sk-button class="tr.btn.setcategory product-btn-set-category">category</sk-button>
                        <% } %>
                        <div class="fm-buy-row">
                            <sk-button id="<portlet:namespace/>addToCartBtn" class="tr.product.btn.addtocart">add to cart</sk-button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<% if (renderRequest.isUserInRole("administrator")) { %>
<product-title-dialog
        id="<portlet:namespace/>titleDialog"
        save-url="${saveNameURL}" type="confirm"
        product-id="${product.id}"
        product-title="${productTitle}"
        optionset-id="${optionSetId}"
        to-body
    >
    <sk-input id="productName" name="productName" sk-required>Name</sk-input>
</product-title-dialog>

<product-image-dialog
        id="<portlet:namespace/>imageDialog"
        save-url="${saveImageURL}" type="confirm"
        product-id="${product.id}"
        to-body
>
    <sk-input type="file" id="file" name="file">Image</sk-input>
</product-image-dialog>


<optionset-image-dialog
        id="<portlet:namespace/>optImageDialog"
        save-url="${saveOptImageURL}" type="confirm"
        to-body
>
    <sk-input type="file" id="optionsetFile" name="optionsetFile">Image</sk-input>
</optionset-image-dialog>

<fieldvalue-dialog
        id="<portlet:namespace/>fieldValueDialog"
        save-url="${saveFieldValueURL}" type="confirm"
        product-id="${product.id}"
        optionset-id="${optionSetId}"
        to-body
>
    <div class="fieldvalue-fields">
        <div class="field-value-wrap">
            <sk-input id="fieldValue" name="fieldValue" sk-required>Value</sk-input>
        </div>
    </div>
</fieldvalue-dialog>
<add-optionset-dialog
        id="<portlet:namespace/>addOptionSetDialog"
        add-url="${addOptionSetURL}" type="confirm"
        product-id="${product.id}"
        to-body
>
    <div>
        <sk-select id="optionSelect" name="optionSelect">
            <% for (ProductOption option : productOptions) { %>
                <option value="<%= String.valueOf(option.getId()) %>"><%= option.getName() %></option>
            <% } %>
        </sk-select>
    </div>
    <div>
        <sk-input id="nameInput" name="nameInput" sk-required>Name</sk-input>
    </div>
</add-optionset-dialog>
<add-fieldvalue-dialog
        id="<portlet:namespace/>addFieldValueDialog"
        add-url="${addFieldValueURL}" type="confirm"
        product-id="${product.id}"
        optionset-id="${optionSetId}"
        to-body
>
    <div class="fieldvalue-fields">
        <div class="infofield-id-wrap">
            <select id="infoFieldId" name="infoFieldId">
                <% for (InfoField field : unsetInfoFields) { %>
                    <option value="<%= String.valueOf(field.getId()) %>" data-field-type="<%= field.getFieldType() %>"><%= field.getName() %></option>
                <% } %>
            </select>
        </div>
        <div class="field-value-wrap">
            <sk-input id="fieldValue" name="fieldValue" sk-required>Value</sk-input>
        </div>
    </div>
</add-fieldvalue-dialog>
<add-product-dialog
        id="<portlet:namespace/>addProductDialog"
        add-url="${addProductURL}" type="confirm"
        to-body
>
    <div>
        <select id="categoryId" name="categoryId">
            <% for (Category category : categories) { %>
                <option value="<%= String.valueOf(category.getId()) %>"><%= category.getName() %></option>
            <% } %>
        </select>
    </div>
    <div>
        <sk-input id="productName" name="productName" sk-required>Name</sk-input>
    </div>
</add-product-dialog>

<set-category-dialog
        id="<portlet:namespace/>setCategoryDialog"
        save-url="${setCategoryURL}" type="confirm"
        product-id="${product.id}"
        cur-category-id="${product.categories[0].id}"
        to-body
>
    <select id="categoryId" name="categoryId">
        <% for (Category category : categories) { %>
            <option value="<%= String.valueOf(category.getId()) %>"><%= category.getName() %></option>
        <% } %>
    </select>
</set-category-dialog>


<% } %>

</fm-product-view>
<script>

    window.fmRegistry = window.fmRegistry || new Registry();

    fmRegistry.wire({
        EventBus,
        Renderer,
        productTr: ${l11nJson},
        productNs: '${renderResponse.namespace}',
        product: ${productJson},
        isAdmin: '<%= renderRequest.isUserInRole("administrator") ? "true" : "false" %>',
        cartBtnSl: '#${renderResponse.namespace}addToCartBtn',
        priceValue: ${priceValue},
        SkConfig: { def: SkConfig, deps: { }, is: 'sk-config'},
        SkButton: { def: SkButton, deps: { }, is: 'sk-button'},
        SkInput: { def: SkInput, deps: { }, is: 'sk-input'},
        SkDialog: { def: SkDialog, deps: { }, is: 'sk-dialog'},
        SkSelect: { def: SkSelect, deps: { }, is: 'sk-select'},
        productConfigDialogCfg: { buttonSl: '#${renderResponse.namespace}configDialogOpenBtn', dialogSl: '#${renderResponse.namespace}configDialog' },
        fieldValueDialogCfg: { buttonsSl: '.prop-btn-edit', dialogSl: '#${renderResponse.namespace}fieldValueDialog' },
        delFieldValueCfg: { buttonsSl: '.prop-btn-delete', deleteUrl: '${delFieldValueURL}', redirectUrl: '${renderURL}' },
        titleDialogCfg: { editButtonSl: '.title-btn-edit', dialogSl: '#${renderResponse.namespace}titleDialog', productNameSl: '#${renderResponse.namespace}productName', redirectUrl: '${renderURL}' },
        imageDialogCfg: { buttonSl: '.image-btn-edit', dialogSl: '#${renderResponse.namespace}imageDialog', redirectUrl: '${renderURL}' },
        optImageDialogCfg: { buttonSl: '.optionset-ava-btn-edit', dialogSl: '#${renderResponse.namespace}optImageDialog', redirectUrl: '${renderURL}' },
        addOptionDialogCfg: { buttonSl: '.optionset-btn-add', dialogSl: '#${renderResponse.namespace}addOptionSetDialog', redirectUrl: '${renderURL}' },
        addProductDialogCfg: { buttonSl: '.product-btn-add', dialogSl: '#${renderResponse.namespace}addProductDialog', redirectUrl: '${renderURL}' },
        addFieldValueDialogCfg: { buttonSl: '.fieldvalue-btn-add', dialogSl: '#${renderResponse.namespace}addFieldValueDialog', redirectUrl: '${renderURL}' },
        setCategoryDialogCfg: { buttonSl: '.product-btn-set-category', dialogSl: '#${renderResponse.namespace}setCategoryDialog', redirectUrl: '${renderURL}' },
        deleteProductCfg: { buttonSl: '.product-btn-delete', deleteUrl: '${deleteOptionSetOrProductURL}', redirectUrl: '${renderURL}', optionSetId: '${optionSetId}', productId: '${product.id}' },
        productClearCacheUrl: '${clearCacheURL}',
        FmProductConfigDialog: { def: FmProductConfigDialog, deps: { 'clearCacheUrl': 'productClearCacheUrl' }, is: 'product-config-dialog'},
        FmProductTitleDialog: { def: FmProductTitleDialog, deps: {}, is: 'product-title-dialog'},
        FmProductImageDialog: { def: FmProductImageDialog, deps: {}, is: 'product-image-dialog'},
        FmOptionSetImageDialog: { def: FmOptionSetImageDialog, deps: {}, is: 'optionset-image-dialog'},
        FmFieldValueDialog: { def: FmFieldValueDialog, deps: {}, is: 'fieldvalue-dialog'},
        FmAddOptionSetDialog: { def: FmAddOptionSetDialog, deps: {}, is: 'add-optionset-dialog'},
        FmAddFieldValueDialog: { def: FmAddFieldValueDialog, deps: {}, is: 'add-fieldvalue-dialog'},
        FmAddProductDialog: { def: FmAddProductDialog, deps: {}, is: 'add-product-dialog'},
        FmSetCategoryDialog: { def: FmSetCategoryDialog, deps: {}, is: 'set-category-dialog'},
        FmProductView: { def: FmProductView, deps: {
            'ns': 'productNs',
            'product': 'product',
            'isAdmin': 'isAdmin',
            'cartBtnSl': 'cartBtnSl',
            'priceValue': 'priceValue',
            'translations': 'productTr',
            'configDialogCfg': 'productConfigDialogCfg',
            'fieldValueDialogCfg': 'fieldValueDialogCfg',
            'titleDialogCfg': 'titleDialogCfg',
            'imageDialogCfg': 'imageDialogCfg',
            'optImageDialogCfg': 'optImageDialogCfg',
            'addOptionDialogCfg': 'addOptionDialogCfg',
            'addFieldValueDialogCfg': 'addFieldValueDialogCfg',
            'delFieldValueCfg': 'delFieldValueCfg',
            'addProductDialogCfg': 'addProductDialogCfg',
            'deleteProductCfg': 'deleteProductCfg',
            'setCategoryDialogCfg': 'setCategoryDialogCfg'
            }, is: 'fm-product-view'}
    });

</script>
