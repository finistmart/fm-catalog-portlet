<%@ page import="com.finistmart.catalog.portlet.dto.Category" %>
<%@ page import="java.util.List" %>
<%@ page session="false" %>
<%@ taglib uri="http://xmlns.jcp.org/portlet_3_0"  prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<portlet:defineObjects />
<portlet:actionURL var="saveSettingsURL" name="saveSettings"></portlet:actionURL>
<portlet:resourceURL var="getSettingsURL" id="getSettings"></portlet:resourceURL>
<portlet:actionURL var="addCategoryURL" name="addCategory"></portlet:actionURL>
<portlet:actionURL var="saveCategoryURL" name="saveCategory"></portlet:actionURL>
<portlet:actionURL var="deleteCategoryURL" name="deleteCategory"></portlet:actionURL>
<portlet:actionURL var="clearCacheURL" name="clearCache"></portlet:actionURL>

<%
    String ctxPath = request.getContextPath();
    List<Category> categories = (List<Category>) renderRequest.getAttribute("categories");
    List<String> settings = (List<String>) renderRequest.getAttribute("settings");
    String settingsJson = (String) renderRequest.getAttribute("settingsJson");
%>

<style>
    .fm-btn-configure {
        background-image: url('<%= ctxPath %>/images/settings_black_192x192.png');
        background-size: contain;
        width: 50px;
        height: 50px;
        float: right;
        margin-top: -50px;
    }
    .mobile-el {
        display: none;
    }
    @media only screen and (max-width: 1000px) { /* mobile*/
        .desktop-el {
            display: none;
        }
        .mobile-el {
            display: block;
        }
    }
    dialog {
        border-width: initial;
        border-style: solid;
        border-color: initial;
        border-image: initial;
        padding: 1em;
        background: white;
    }
    dialog label {
        min-width: 250px;
    }
</style>

<sk-config
        theme="antd"
        base-path="<%= request.getContextPath()%>/fm-catalog/node_modules/skinny-widgets/src"
></sk-config>

<% if (renderRequest.isUserInRole("administrator")) { %>
    <fm-categories-config-dialog id="${renderResponse.getNamespace()}configDialog" ns="${renderResponse.getNamespace()}"
          saveUrl="${saveSettingsURL.toString()}" settingsUrl="${getSettingsURL.toString()}"
          type="confirm" to-body
    >
        <h3>Settings</h3>
        <div>
            <% for (String settingName : settings) { %>
            <div>
                <label><%= settingName %></label>
                <sk-input type="text" name="<%= settingName %>" value="<%= (String) renderRequest.getAttribute(settingName) %>"></sk-input>
            </div>
            <% } %>
        </div>

        <template id="FmCategoriesConfigDialogFooterTpl">
            <div>
                <sk-button action="clearCache">Clear Cache</sk-button>
                <sk-button action="cancel">Cancel</sk-button>
                <sk-button action="save" button-type="primary">Save</sk-button>
            </div>
        </template>
    </fm-categories-config-dialog>
    <button class="fm-btn-configure" id="${renderResponse.getNamespace()}configDialogOpenBtn"></button>
<% } %>



<fm-categories-view>

<% if (renderRequest.isUserInRole("administrator")) { %>
    <sk-button id="<portlet:namespace/>addCategoryBtn" class="tr.category.btn.add">add</sk-button>
    <sk-button id="<portlet:namespace/>editCategoryBtn" class="tr.category.btn.edit">edit</sk-button>
    <sk-button id="<portlet:namespace/>deleteCategoryBtn" class="tr.category.btn.delete">delete</sk-button>
<% } %>

<sk-tree class="desktop-el" expanded="false" link-tpl-str="${categoryLinkTplStr}" tree-data='${categoriesJson}' selected-id="${selectedCategoryId}"></sk-tree>

<label for="<portlet:namespace/>categorySelectMob" class="tr.category.label.select.category mobile-el">Select category</label>
<sk-select class="mobile-el" id="<portlet:namespace/>categorySelectMob">
    <option value="0">--</option>
    <% for (Category category : categories) { %>
    <option value="<%= String.valueOf(category.getId()) %>"><%= category.getName() %></option>
    <% } %>
</sk-select>


<% if (renderRequest.isUserInRole("administrator")) { %>
<add-category-dialog
        id="<portlet:namespace/>addCategoryDialog"
        add-url="${addCategoryURL}" type="confirm"
        to-body
>
    <div>
        <select id="parentCategoryId" name="parentCategoryId">
            <option value="0">--</option>
            <% for (Category category : categories) { %>
                <option value="<%= String.valueOf(category.getId()) %>"><%= category.getName() %></option>
            <% } %>
        </select>
    </div>
    <sk-input id="nameInput" name="nameInput" sk-required>Name</sk-input>

</add-category-dialog>

<edit-category-dialog
        id="<portlet:namespace/>editCategoryDialog"
        save-url="${saveCategoryURL}" type="confirm"
        to-body
>
    <div>
        <select id="editCategoryId" name="editCategoryId">
            <% for (Category category : categories) { %>
                <option value="<%= String.valueOf(category.getId()) %>" data-parent-id="<%= String.valueOf(category.getParentId()) %>" ><%= category.getName() %></option>
            <% } %>
        </select>
    </div>
    <div>
        <sk-input id="nameInput" name="nameInput" sk-required>New Name</sk-input>
    </div>
    <div>
        <select id="parentCategoryId" name="parentCategoryId">
            <option value="0">--</option>
            <% for (Category category : categories) { %>
            <option value="<%= String.valueOf(category.getId()) %>"><%= category.getName() %></option>
            <% } %>
        </select>
    </div>
</edit-category-dialog>

<delete-category-dialog
        id="<portlet:namespace/>deleteCategoryDialog"
        delete-url="${deleteCategoryURL}" type="confirm"
        to-body
>
    <select id="categoryId" name="categoryId">
        <% for (Category category : categories) { %>
            <option value="<%= String.valueOf(category.getId()) %>"><%= category.getName() %></option>
        <% } %>
    </select>

</delete-category-dialog>


<% } %>

</fm-categories-view>
<script src="<%= ctxPath %>/fm-catalog/fm-catalog-portlet-bundle.js"></script>
<script>


    window.fmRegistry = window.fmRegistry || new Registry();

    fmRegistry.wire({
        EventBus,
        Renderer,
        categoriesTr: ${l11nJson},
        categoriesNs: '${renderResponse.namespace}',
        isAdmin: '<%= renderRequest.isUserInRole("administrator") ? "true" : "false" %>',
        SkConfig: { def: SkConfig, deps: {  }, is: 'sk-config'},
        SkTree: { def: SkTree, deps: { }, is: 'sk-tree'},
        SkButton: { def: SkButton, deps: {  }, is: 'sk-button'},
        SkInput: { def: SkInput, deps: { }, is: 'sk-input'},
        SkSelect: { def: SkSelect, deps: { }, is: 'sk-select'},
        categoriesCfg: { 'categorySelectMob': '#<portlet:namespace/>categorySelectMob' },
        categoriesConfigDialogCfg: {  buttonSl: '#${renderResponse.namespace}configDialogOpenBtn',
            dialogSl: '#${renderResponse.namespace}configDialog' },
        categoriesClearCacheUrl: '${clearCacheURL}',
        FmCategoriesConfigDialog: { def: FmCategoriesConfigDialog, deps: {
            'clearCacheUrl': 'categoriesClearCacheUrl'
        }, is: 'fm-categories-config-dialog'},
        FmAddCategoryDialog: { def: FmAddCategoryDialog, deps: {}, is: 'add-category-dialog'},
        FmEditCategoryDialog: { def: FmEditCategoryDialog, deps: {}, is: 'edit-category-dialog'},
        FmDeleteCategoryDialog: { def: FmDeleteCategoryDialog, deps: {}, is: 'delete-category-dialog'},
        addCategoryDialogCfg: { buttonSl: '#<portlet:namespace/>addCategoryBtn', dialogSl: '#${renderResponse.namespace}addCategoryDialog', redirectUrl: '${renderURL}' },
        editCategoryDialogCfg: { buttonSl: '#<portlet:namespace/>editCategoryBtn', dialogSl: '#${renderResponse.namespace}editCategoryDialog', redirectUrl: '${renderURL}' },
        deleteCategoryDialogCfg: { buttonSl: '#<portlet:namespace/>deleteCategoryBtn', dialogSl: '#${renderResponse.namespace}deleteCategoryDialog', redirectUrl: '${renderURL}' },
        FmCategoriesView: {
            def: FmCategoriesView, deps: {
                'categoriesCfg': 'categoriesCfg',
                'translations': 'categoriesTr',
                'ns': 'categoriesNs',
                'isAdmin': 'isAdmin',
                'configDialogCfg': 'categoriesConfigDialogCfg',
                'addCategoryDialogCfg': 'addCategoryDialogCfg',
                'editCategoryDialogCfg': 'editCategoryDialogCfg',
                'deleteCategoryDialogCfg': 'deleteCategoryDialogCfg'
            }, is: 'fm-categories-view'
        }
    });

</script>
