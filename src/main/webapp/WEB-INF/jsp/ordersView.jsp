<%@ page import="java.util.List" %>
<%@ page session="false" %>
<%@ taglib uri="http://xmlns.jcp.org/portlet_3_0"  prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<portlet:defineObjects />
<portlet:actionURL var="saveSettingsURL" name="saveSettings"></portlet:actionURL>
<portlet:actionURL var="saveOrderURL" name="saveOrder"></portlet:actionURL>
<portlet:resourceURL var="getSettingsURL" id="getSettings"></portlet:resourceURL>
<portlet:renderURL var="renderURL"></portlet:renderURL>
<portlet:actionURL var="clearCacheURL" name="clearCache"></portlet:actionURL>
<%
    String ctxPath = request.getContextPath();
    List<String> settings = (List<String>) renderRequest.getAttribute("settings");
    String settingsJson = (String) renderRequest.getAttribute("settingsJson");
%>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/antd/3.19.8/antd.css" />
<style>
    .fm-btn-configure {
        background-image: url('<%= ctxPath %>/images/settings_black_192x192.png');
        background-size: contain;
        width: 50px;
        height: 50px;
        float: right;
        margin-top: -50px;
    }
    .category-select {
        display: none;
    }
    @media only screen and (max-width: 1000px) { /* mobile*/
        fm-category-tree {
            display: none;
        }
        .category-mobile-select {
            display: block;
        }
    }
    dialog:not([open]) {
        display: none;
    }
    th.sort.sort-desc::after {
        content: '▼';
    }
    th.sort.sort-asc::after {
        content: '▲';
    }

</style>


<% if (renderRequest.isUserInRole("administrator")) { %>
    <fm-orders-config-dialog ns="${renderResponse.namespace}" id="${renderResponse.namespace}configDialog" type="confirm"
                          saveUrl="${saveSettingsURL.toString()}" settingsUrl="${getSettingsURL.toString()}" to-body>
        <h3>Settings</h3>
        <div>
            <% for (String settingName : settings) { %>
            <div>
                <label><%= settingName %></label>
                <input type="text" name="<%= settingName %>" value="<%= (String) renderRequest.getAttribute(settingName) %>" />
            </div>
            <% } %>
        </div>
        <template id="FmOrdersConfigDialogFooterTpl">
            <div>
                <sk-button action="clearCache">Clear Cache</sk-button>
                <sk-button action="cancel">Cancel</sk-button>
                <sk-button action="save" button-type="primary">Save</sk-button>
            </div>
        </template>
    </fm-orders-config-dialog>

    <button class="fm-btn-configure" id="${renderResponse.namespace}configDialogOpenBtn"></button>
<% } %>

<sk-config
        theme="antd"
        base-path="<%= request.getContextPath()%>/fm-catalog/node_modules/skinny-widgets/src"
></sk-config>

<fm-orders-view>
    <gridy-table base-path="<%= ctxPath %>/fm-catalog/node_modules/gridy-grid/src" theme="antd" id="<portlet:namespace/>ordersTable" pgref="<portlet:namespace/>ordersPager"></gridy-table>
    <gridy-pager base-path="<%= ctxPath %>/fm-catalog/node_modules/gridy-grid/src" theme="antd" per-page="20" id="<portlet:namespace/>ordersPager"></gridy-pager>
</fm-orders-view>

<fm-edit-order-dialog
        id="<portlet:namespace/>editOrderDialog"
        save-url="${saveOrderURL}" type="confirm"
>
    <sk-form action="${saveOrderURL}" method="POST" id="orderForm" enctype="auto">
        <span slot="fields">
            <style>
                label {
                    width: 150px;
                    display: inline-block;
                    vertical-align: top;
                }
            </style>
            <div>
                <label id="buyerIdent" class="sk-prop-in-buyer-ident"></label>
            </div>
            <div>
                <label for="orderData" class="tr-order-label-notes">Data</label>
                <textarea id="orderData" name="orderData" class="sk-prop-in-order-data"></textarea>
            </div>
            <div>
                <label for="orderNotes" class="tr-order-label-notes">Notes</label>
                <textarea id="orderNotes" name="orderNotes" class="sk-prop-in-order-notes"></textarea>
            </div>
            <div>
                <label for="orderStatus" class="tr-order-label-status">Status</label>
                <select id="orderStatus" name="orderStatus" class="sk-prop-cb-order-status">
                    <option value="NEW">NEW</option>
                    <option value="PROCESSING">PROCESSING</option>
                    <option value="DONE">DONE</option>
                </select>
            </div>
            <div>
                <label for="orderNotes" class="tr-order-created">Created</label>
                <span id="created" class="sk-prop-in-order-created"></span>
            </div>
            <div>
                <label for="orderNotes" class="tr-order-updated">Updated</label>
                <span id="updated" class="sk-prop-in-order-updated"></span>
                <sk-input type="hidden" name="orderId" id="orderId" class="sk-prop-at-order-id_value"></sk-input>
            </div>
        </span>
    </sk-form>
</fm-edit-order-dialog>
<script>

    window.fmRegistry = window.fmRegistry || new Registry();

    fmRegistry.wire({
        EventBus,
        Renderer,
        ordersTr: { val: ${l11nJson} },
        ordersNs: { val: '${renderResponse.namespace}' },
        orders: { val: ${ordersJson} },
        isAdmin: { val: '<%= renderRequest.isUserInRole("administrator") ? "true" : "false" %>' },
        ordersConfigDialogCfg: { buttonSl: '#<portlet:namespace/>configDialogOpenBtn', dialogSl: '#<portlet:namespace/>configDialog' },
        SkConfig: { def: SkConfig, deps: { 'renderer': Renderer }, is: 'sk-config'},
        SkTree: { def: SkTree, deps: { 'renderer': Renderer }, is: 'sk-tree'},
        SkButton: { def: SkButton, deps: { 'renderer': Renderer }, is: 'sk-button'},
        SkInput: { def: SkInput, deps: { 'renderer': Renderer }, is: 'sk-input'},
        SkForm: { def: SkForm, deps: { 'renderer': Renderer }, is: 'sk-form'},
        GridyTable: { def: GridyTable, deps: { 'renderer': Renderer }, is: 'gridy-table'},
        FmEditOrderDialog: { def: FmEditOrderDialog, deps: {}, is: 'fm-edit-order-dialog'},
        ordersClearCacheUrl: '${clearCacheURL}',
        FmOrdersConfigDialog: { def: FmOrdersConfigDialog, deps: {
            'clearCacheUrl': 'ordersClearCacheUrl'
        }, is: 'fm-orders-config-dialog'},
        editOrderDialogCfg: { val: { dialogSl: '#<portlet:namespace/>editOrderDialog', redirectUrl: '${renderURL}' }},
        FmOrdersView: {
            def: FmOrdersView, deps: {
                'translations': 'ordersTr',
                'ns': 'ordersNs',
                'isAdmin': 'isAdmin',
                'configDialogCfg': 'ordersConfigDialogCfg',
                'orders': 'orders',
                'editOrderDialogCfg': 'editOrderDialogCfg',
            }, is: 'fm-orders-view'
        }
    });



</script>
