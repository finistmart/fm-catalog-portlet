<%@ page import="java.util.List" %>
<%@ page session="false" %>
<%@ taglib uri="http://xmlns.jcp.org/portlet_3_0"  prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<portlet:defineObjects />
<portlet:actionURL var="saveSettingsURL" name="saveSettings"></portlet:actionURL>
<portlet:resourceURL var="getSettingsURL" id="getSettings"></portlet:resourceURL>
<portlet:actionURL var="clearCacheURL" name="clearCache"></portlet:actionURL>
<%
    String ctxPath = request.getContextPath();
    List<String> settings = (List<String>) renderRequest.getAttribute("settings");
%>
<style>
    .fm-btn-configure {
        background-image: url('<%= ctxPath %>/images/settings_black_192x192.png');
        background-size: contain;
        width: 50px;
        height: 50px;
        float: right;
        margin-top: -50px;
    }
    dialog {
        border-width: initial;
        border-style: solid;
        border-color: initial;
        border-image: initial;
        padding: 1em;
        background: white;
    }
    dialog label {
        min-width: 250px;
    }
</style>

<sk-config
        theme="antd"
        base-path="<%= request.getContextPath()%>/fm-catalog/node_modules/skinny-widgets/src/"
></sk-config>


<% if (renderRequest.isUserInRole("administrator")) { %>

<fm-catalog-config-dialog
        id="${renderResponse.namespace}configDialog"
        ns="${renderResponse.namespace}"
        saveUrl="${saveSettingsURL.toString()}"
        settingsUrl="${getSettingsURL.toString()}"
        type="confirm" to-body
>
    <h3>Settings</h3>
    <div>
        <% for (String settingName : settings) { %>
            <div>
                <label><%= settingName %></label>
                <sk-input type="text" name="<%= settingName %>" value="<%= (String) renderRequest.getAttribute(settingName) %>"></sk-input>
            </div>
        <% } %>
    </div>
    <template id="FmCatalogConfigDialogFooterTpl">
        <div>
            <sk-button action="clearCache">Clear Cache</sk-button>
            <sk-button action="cancel">Cancel</sk-button>
            <sk-button action="save" button-type="primary">Save</sk-button>
        </div>
    </template>
</fm-catalog-config-dialog>

<button class="fm-btn-configure" id="${renderResponse.namespace}configDialogOpenBtn"></button>

<% } %>


<style>
    .sk-box-container {
        display: flex;
        flex-wrap: wrap;
    }
    .fm-product-title,.fm-product-bpane,.fm-product-price  {
        text-align: center;
    }
    .fm-product-price {
        font-size: 2em;
    }
    fm-product-pager {
        display: inline-block;
    }
    fm-product-pager ul {
        list-style-type: none;
    }
    fm-product-pager ul > li {
        float: left;
        margin-left: 5px;
        text-decoration: underline;
        cursor: pointer;
    }
    fm-product-pager ul > li.ant-pagination-item-active {
        text-decoration: none;
    }
</style>

<fm-catalog-view>
    <fm-product-pager id="<portlet:namespace/>productTopPager"
                      total-results="${total}" total-pages="${totalPages}" page-num="${pageNum}"
                      base-path="<%= request.getContextPath()%>/fm-catalog/node_modules/gridy-grid/src/"></fm-product-pager>
    <br />
    <fm-product-grid class="sk-box-container"
                     product-data='${productsJson}' product-noimage-path="<%= ctxPath %>/fm-catalog/product-noimage.svg"
                     product-link-tpl-str="${productLinkTplStr}"></fm-product-grid>
    <fm-product-pager id="<portlet:namespace/>productBottomPager"
                      total-results="${total}" total-pages="${totalPages}" page-num="${pageNum}"
                      base-path="<%= request.getContextPath()%>/fm-catalog/node_modules/gridy-grid/src/"></fm-product-pager>
    <br />
</fm-catalog-view>
<script>

    window.fmRegistry = window.fmRegistry || new Registry();

    fmRegistry.wire({
        EventBus,
        Renderer,
        catalogTr: { val: ${l11nJson}},
        catalogNs: { val: '${renderResponse.namespace}' },
        isAdmin: { val: '<%= renderRequest.isUserInRole("administrator") ? "true" : "false" %>' },
        SkConfig: { def: SkConfig, deps: { }, is: 'sk-config'},
        SkButton: { def: SkButton, deps: { }, is: 'sk-button'},
        SkInput: { def: SkInput, deps: { }, is: 'sk-input'},
        SkDialog: { def: SkDialog, deps: { }, is: 'sk-dialog'},
        FmProductGrid: { def: FmProductGrid, deps: {
                'renderer': Renderer
            }, is: 'fm-product-grid'},
        FmProductItem: { def: FmProductItem, deps: {
                'renderer': Renderer
            }, is: 'fm-product-item'},
        FmProductPager: { def: FmProductPager, deps: { }, is: 'fm-product-pager'},
        catalogConfigDialogCfg: { buttonSl: '#${renderResponse.namespace}configDialogOpenBtn',
                dialogSl: '#${renderResponse.namespace}configDialog' },
        catalogClearCacheUrl: '${clearCacheURL}',
        FmCatalogConfigDialog: { def: FmCatalogConfigDialog, deps: {
                'renderer': Renderer,
                'clearCacheUrl': 'catalogClearCacheUrl'
            }, is: 'fm-catalog-config-dialog'},
        FmCatalogView: {
            def: FmCatalogView, deps: {
                'translations': 'catalogTr',
                'ns': 'catalogNs',
                'isAdmin': 'isAdmin',
                'configDialogCfg': 'catalogConfigDialogCfg'
            }, is: 'fm-catalog-view'
        }
    });

</script>
