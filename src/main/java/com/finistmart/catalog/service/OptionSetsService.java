package com.finistmart.catalog.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.finistmart.catalog.HttpClient;
import com.finistmart.catalog.portlet.cache.CacheFetcher;
import com.finistmart.catalog.portlet.cache.CacheHandler;
import com.finistmart.catalog.portlet.dto.Product;
import com.finistmart.catalog.portlet.dto.ProductOptionSet;
import com.finistmart.catalog.portlet.dto.ProductOptionSetAdd;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;

@ApplicationScoped
public class OptionSetsService {

    private static final Logger logger = LoggerFactory.getLogger(OptionSetsService.class);

    public static String OPTIONSETS_CACHE_NAME = "optionsets";

    private final CacheHandler<Long, ProductOptionSet> cacheHandler = new CacheHandler<>(new CacheFetcher<>());

    @Inject
    private ProductOptionsService productOptionsService;

    @Inject
    private ProductsService productsService;

    @Inject
    private JsonMapper jsonMapper;

    private ObjectMapper mapper() {
        return jsonMapper.mapper();
    }

    public ProductOptionSet getOptionSet(HttpClient client, Long optionSetId) {
        ProductOptionSet optionSet = cacheHandler.getEntityFromCache(OPTIONSETS_CACHE_NAME)
            .orFromSource(() -> {
                return this.fetchOptionSet(client, optionSetId);
            }).usingCacheKey(optionSetId)
        ;

        return optionSet;
    }

    public ProductOptionSet fetchOptionSet(HttpClient client, Long optionSetId) {
        ProductOptionSet optionSet = new ProductOptionSet();
        String requestUrl = "/api/optionsets";
        if (optionSetId != null) {
            requestUrl += "/" + String.valueOf(optionSetId);
        }
        try {
            String responseBody = client.request(requestUrl, "GET");
            if (responseBody != null) {
                optionSet = mapper().readValue(responseBody, new TypeReference<ProductOptionSet>() {});
            } else {
                return optionSet;
            }
        } catch (IOException e) {
            logger.error("Error adding field value ", e);
        }

        return optionSet;
    }

    public ProductOptionSet saveOptionSet(HttpClient client, ProductOptionSet optionSet) {

        try {
            String body = mapper().writeValueAsString(optionSet);
            String responseBody = client.request("/api/optionsets/" + optionSet.getId() + "/save", "PUT", body);
            optionSet = mapper().readValue(responseBody, new TypeReference<ProductOptionSet>() { });
            cacheHandler.evict(OPTIONSETS_CACHE_NAME, optionSet.getId());
        } catch (IOException e) {
            logger.error("Error adding field value ", e);
        }

        return optionSet;
    }

    public ProductOptionSet deleteOptionSet(HttpClient client, ProductOptionSet optionSet) {

        try {
            client.request("/api/optionsets/" + optionSet.getId() + "/delete", "DELETE");
            cacheHandler.evict(OPTIONSETS_CACHE_NAME, optionSet.getId());
            if (optionSet.getProduct() != null) {
                this.productsService.clearCacheForProduct(optionSet.getProduct());
            }
        } catch (IOException e) {
            logger.error("Error adding field value ", e);
        }

        return optionSet;
    }

    public ProductOptionSet addOptionSet(HttpClient client, Long productId, Long optionId, String name) {
        ProductOptionSet optionSet = null;
        String requestUrl = "/api/optionsets/add";
        ProductOptionSetAdd optionSetAdd = new ProductOptionSetAdd(optionId, productId, name);
        Product product = productsService.getProduct(client, productId);
        try {
            String body = mapper().writeValueAsString(optionSetAdd);
            String responseBody = client.request(requestUrl, "POST", body);
            optionSet = mapper().readValue(responseBody, new TypeReference<ProductOptionSet>() { });
            cacheHandler.evict(OPTIONSETS_CACHE_NAME, optionSet.getId());
            productOptionsService.clearOptionsCache(String.valueOf(productId));
            productsService.clearCacheForProduct(product);
        } catch (IOException e) {
            logger.error("Error adding option set ", e);
        }

        return optionSet;
    }

}
