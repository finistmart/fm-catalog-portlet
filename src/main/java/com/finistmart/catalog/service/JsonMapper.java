package com.finistmart.catalog.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class JsonMapper {

    private ObjectMapper mapper = null;

    public ObjectMapper mapper() {
        if (mapper == null) {
            mapper = new ObjectMapper()
                    .registerModule(new JavaTimeModule());
        }
        return mapper;
    }

    @PostConstruct
    public void initMapper() {
        this.mapper();
    }
}
