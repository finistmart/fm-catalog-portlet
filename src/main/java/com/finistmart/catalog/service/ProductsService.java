package com.finistmart.catalog.service;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.finistmart.catalog.HttpClient;
import com.finistmart.catalog.portlet.cache.CacheFetcher;
import com.finistmart.catalog.portlet.cache.CacheHandler;
import com.finistmart.catalog.portlet.dto.Category;
import com.finistmart.catalog.portlet.dto.OptionValueAdd;
import com.finistmart.catalog.portlet.dto.ProductFieldValue;

import com.finistmart.catalog.portlet.dto.ProductImage;
import com.finistmart.catalog.portlet.dto.ProductOptionValue;
import com.finistmart.catalog.portlet.dto.ProductValueAdd;

import com.finistmart.catalog.portlet.dto.Product;

import com.finistmart.catalog.portlet.dto.ProductsResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.joining;

@ApplicationScoped
public class ProductsService {

    private static final Logger logger = LoggerFactory.getLogger(ProductsService.class);

    public static String PRODUCTS_CACHE_NAME = "products";
    public static String PRODUCTSBYCAT_CACHE_NAME = "products_byCategoryId";
    public static String OPTIONVALUES_CACHE_NAME = "optionvalues_byOptionSetId";


    private final CacheHandler<String, ProductsResult> productsCacheHandler = new CacheHandler<>(new CacheFetcher<>());
    private final CacheHandler<String, Product> productCacheHandler = new CacheHandler<>(new CacheFetcher<>());
    private final CacheHandler<String, List<ProductOptionValue>> optionValuesCacheHandler = new CacheHandler<>(new CacheFetcher<>());


    @Inject
    private JsonMapper jsonMapper;

    private ObjectMapper mapper() {
        return jsonMapper.mapper();
    }

    public void clearCache() {
        this.productsCacheHandler.clear(PRODUCTSBYCAT_CACHE_NAME);
        this.productCacheHandler.clear(PRODUCTS_CACHE_NAME);
        this.optionValuesCacheHandler.clear(OPTIONVALUES_CACHE_NAME);
    }

    public ProductsResult getProducts(HttpClient client, Long categoryId, Integer pageNum, Integer pageSize) {
        ProductsResult result = productsCacheHandler.getEntityFromCache(PRODUCTSBYCAT_CACHE_NAME)
            .orFromSource(() -> {
                return this.fetchProducts(client, categoryId, pageNum, pageSize);
            }).usingCacheKey("categoryId_" + String.valueOf(categoryId) + "_pageNum_" + String.valueOf(pageNum) + "_pageSize_" + String.valueOf(pageSize))
        ;
        return result;
    }

    public ProductsResult fetchProducts(HttpClient client, Long categoryId, Integer pageNum, Integer pageSize) {
        ProductsResult result = null;
        String requestUrl = "/api/products";
        Map<String, String> params = new HashMap<>();
        if (categoryId != null) {
            params.put("categoryId", String.valueOf(categoryId));
        }
        if (pageSize != null) {
            params.put("pageSize", String.valueOf(pageSize));
        }
        if (pageNum != null) {
            params.put("pageNum", String.valueOf(pageNum));
        }
        String paramsString = params.entrySet()
                .stream()
                .map(e -> e.getKey()+"="+e.getValue())
                .collect(joining("&"));
        try {
            String responseBody = client.request(requestUrl + "?" + paramsString, "GET");
            result = mapper().readValue(responseBody, new TypeReference<ProductsResult>() { });
        } catch (IOException e) {
            logger.error("Error getting products list", e);
        }

        return result;
    }

    public Product getProduct(HttpClient client, Long productId) {
        Product product = productCacheHandler.getEntityFromCache(PRODUCTS_CACHE_NAME)
            .orFromSource(() -> {
                return this.fetchProduct(client, productId);
            }).usingCacheKey(String.valueOf(productId))
        ;

        return product;
    }

    public Product fetchProduct(HttpClient client, Long productId) {
        Product product = null;
        String requestUrl = "/api/products";
        if (productId != null) {
            requestUrl += "/" + String.valueOf(productId);
        }
        try {
            String responseBody = client.request(requestUrl, "GET");
            product = mapper().readValue(responseBody, new TypeReference<Product>() { });
        } catch (IOException e) {
            logger.error("Error getting product by id ", e);
        }

        return product;
    }

    public Product addProduct(HttpClient client, Product product, Long categoryId) {
        String requestUrl = "/api/products/add";
        if (categoryId != null) {
            requestUrl += "?categoryId=" + String.valueOf(categoryId);
        }
        try {
            String body = mapper().writeValueAsString(product);
            String responseBody = client.request(requestUrl, "POST", body);
            product = mapper().readValue(responseBody, new TypeReference<Product>() { });
            productsCacheHandler.evict(PRODUCTSBYCAT_CACHE_NAME, String.valueOf(categoryId));
            productCacheHandler.evict(PRODUCTS_CACHE_NAME, String.valueOf(product.getId()));
        } catch (IOException e) {
            logger.error("Error adding product ", e);
        }

        return product;
    }

    public Product saveProduct(HttpClient client, Product product) {
        String requestUrl = "/api/products/";
        if (product != null) {
            requestUrl += product.getId() + "/save";
        }

        try {
            String body = mapper().writeValueAsString(product);
            String responseBody = client.request(requestUrl, "PUT", body);
            product = mapper().readValue(responseBody, new TypeReference<Product>() { });
            this.clearCacheForProduct(product);
        } catch (IOException e) {
            logger.error("Error saving product ", e);
        }

        return product;
    }

    public Product setCategory(HttpClient client, Product product, Category category) {
        String requestUrl = "/api/products/";
        if (product != null) {
            requestUrl += product.getId() + "/setcategory";
        }

        try {
            String body = mapper().writeValueAsString(category);
            String responseBody = client.request(requestUrl, "PUT", body);
            product = mapper().readValue(responseBody, new TypeReference<Product>() { });

            productsCacheHandler.clear(PRODUCTSBYCAT_CACHE_NAME);
            productCacheHandler.clear(PRODUCTS_CACHE_NAME);
        } catch (IOException e) {
            logger.error("Error setting product category", e);
        }

        return product;
    }

    public Product deleteProduct(HttpClient client, Product product) {
        String requestUrl = "/api/products/";
        if (product != null) {
            requestUrl += product.getId() + "/delete";
        }
        try {
            client.request(requestUrl, "GET");
            this.clearCacheForProduct(product);
        } catch (IOException e) {
            logger.error("Error saving product ", e);
        }

        return product;
    }

    public void clearCacheForProduct(Product product) {
        for (Category category: product.getCategories()) {
            productsCacheHandler.evict(PRODUCTSBYCAT_CACHE_NAME, String.valueOf(category.getId()));
        }
        productCacheHandler.evict(PRODUCTS_CACHE_NAME, String.valueOf(product.getId()));
    }

    public ProductImage saveProductImage(HttpClient client, Product product) {
        String requestUrl = "/api/productimages/";
        ProductImage productImage = product.getImage();
        if (productImage != null) {
            requestUrl += productImage.getId() + "/save?productId=" + product.getId();
        } else {
            requestUrl += "/add?productId=" + product.getId();
        }

        try {
            String body = mapper().writeValueAsString(productImage);
            String responseBody = client.request(requestUrl, productImage != null ? "PUT" : "POST", body);
            productImage = mapper().readValue(responseBody, new TypeReference<ProductImage>() { });
        } catch (IOException e) {
            logger.error("Error saving product image", e);
        }

        return productImage;
    }

    public List<ProductOptionValue> getOptionValues(HttpClient client, Long optionSetId) {

        List<ProductOptionValue> optionValues = optionValuesCacheHandler.getEntityFromCache(OPTIONVALUES_CACHE_NAME)
            .orFromSource(() -> {
                return this.fetchOptionValues(client, optionSetId);
            }).usingCacheKey(String.valueOf(optionSetId))
        ;

        return optionValues;
    }

    public List<ProductOptionValue> fetchOptionValues(HttpClient client, Long optionSetId) {
        List<ProductOptionValue> optionValues = new ArrayList<>();
        String requestUrl = "/api/optionvalues/byoptionset";

        if (optionSetId != null) {
            requestUrl += "?optionSetId=" + String.valueOf(optionSetId);
        }

        try {
            String responseBody = client.request(requestUrl, "GET");
            optionValues = mapper().readValue(responseBody, new TypeReference<List<ProductOptionValue>>() { });
        } catch (IOException e) {
            logger.error("Error saving product ", e);
        }

        return optionValues;
    }

    public ProductFieldValue saveFieldValue(HttpClient client, Product product, String fieldName, String value) {
        List<ProductFieldValue> fieldValues = product.getFieldValues();
        ProductFieldValue fieldValue = fieldValues.stream()
                .filter((fv) -> fieldName.equals(fv.getInfoField().getName())).findFirst().orElseGet(null);

        String requestUrl = "/api/fieldvalues/";

        if (fieldValue != null) {
            fieldValue.setProduct(product);
            fieldValue.setValue(value);
            requestUrl += fieldValue.getId() + "/save";
        }

        try {
            String body = mapper().writeValueAsString(fieldValue);
            String responseBody = client.request(requestUrl, "PUT", body);
            fieldValue = mapper().readValue(responseBody, new TypeReference<ProductFieldValue>() { });
            this.clearCacheForProduct(product);
        } catch (IOException e) {
            logger.error("Error saving field value ", e);
        }

        return fieldValue;
    }

    public ProductOptionValue saveOptionValue(HttpClient client, ProductOptionValue optionValue, String newValue) {

        try {
            String body = mapper().writeValueAsString(optionValue);
            String responseBody = client.request("/api/optionvalues/", "PUT", body);
            optionValue = mapper().readValue(responseBody, new TypeReference<ProductOptionValue>() { });
            optionValuesCacheHandler.evict(OPTIONVALUES_CACHE_NAME, String.valueOf(optionValue.getProductOptionSet().getId()));
        } catch (IOException e) {
            logger.error("Error saving option value ", e);
        }

        return optionValue;
    }

    public ProductOptionValue addOptionValue(HttpClient client, Long optionSetId, Long infoFieldId, String fieldValueParam) {
        ProductOptionValue optionValue = null;

        String requestUrl = "/api/optionvalues/add";
        OptionValueAdd optionValueTO = new OptionValueAdd(optionSetId, infoFieldId, fieldValueParam);

        try {
            String body = mapper().writeValueAsString(optionValueTO);
            String responseBody = client.request(requestUrl, "POST", body);
            optionValue = mapper().readValue(responseBody, new TypeReference<ProductOptionValue>() { });
            optionValuesCacheHandler.clear(OPTIONVALUES_CACHE_NAME);
        } catch (IOException e) {
            logger.error("Error adding option value ", e);
        }

        return optionValue;
    }

    public ProductFieldValue addFieldValue(HttpClient client, Long productId, Long infoFieldId, String fieldValueParam) {
        ProductFieldValue fieldValue = null;
        String requestUrl = "/api/fieldvalues/add";
        Product product = getProduct(client, productId);
        ProductValueAdd productValueTO = new ProductValueAdd(product.getId(), infoFieldId, fieldValueParam);

        try {
            String body = mapper().writeValueAsString(productValueTO);
            String responseBody = client.request(requestUrl, "POST", body);
            fieldValue = mapper().readValue(responseBody, new TypeReference<ProductFieldValue>() { });
            this.clearCacheForProduct(product);
        } catch (IOException e) {
            logger.error("Error adding field value ", e);
        }

        return fieldValue;
    }

    public ProductOptionValue delOptionValue(HttpClient client, Long valueId) {
        ProductOptionValue optionValue = null;

        String requestUrl = "/api/optionvalues/" + String.valueOf(valueId) + "/delete";

        try {
            String responseBody = client.request(requestUrl, "DELETE");
            productsCacheHandler.clear(PRODUCTSBYCAT_CACHE_NAME);
            productCacheHandler.clear(PRODUCTS_CACHE_NAME);
            productCacheHandler.clear(OPTIONVALUES_CACHE_NAME);
        } catch (IOException e) {
            logger.error("Error deleting option value ", e);
        }

        return optionValue;
    }

    public ProductFieldValue delFieldValue(HttpClient client, Long valueId) {
        ProductFieldValue fieldValue = null;
        String requestUrl = "/api/fieldvalues/" + String.valueOf(valueId) + "/delete";

        try {
            String responseBody = client.request(requestUrl, "DELETE");
            productsCacheHandler.clear(PRODUCTSBYCAT_CACHE_NAME);
            productCacheHandler.clear(PRODUCTS_CACHE_NAME);
        } catch (IOException e) {
            logger.error("Error deleting field value ", e);
        }

        return fieldValue;
    }
}
