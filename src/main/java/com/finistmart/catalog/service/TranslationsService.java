package com.finistmart.catalog.service;


import com.finistmart.catalog.portlet.cache.CacheFetcher;
import com.finistmart.catalog.portlet.cache.CacheHandler;
import com.finistmart.catalog.portlet.i18n.Utf8ResourceBundleControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

@ApplicationScoped
public class TranslationsService {

    private static final Logger logger = LoggerFactory.getLogger(TranslationsService.class);

    private final CacheHandler<String, Map<String, String>> cacheHandler = new CacheHandler<>(new CacheFetcher<>());


    public Map<String, String> getTranslationsMap(RenderRequest request, RenderResponse response, String portletName) {
        String langSuffix = response.getLocale() != null ? response.getLocale().getLanguage() : "";
        Map<String, String> translations = cacheHandler.getEntityFromCache("translations" + langSuffix)
            .orFromSource(() -> {
                return this.loadTranslationsMap(request, response, portletName);
            }).usingCacheKey(portletName)
        ;
        return translations;
    }

    public Map<String, String> loadTranslationsMap(RenderRequest request, RenderResponse response, String portletName) {
        Map<String, String> translations = new HashMap<>();
        ResourceBundle.Control utf8Control =
                new Utf8ResourceBundleControl();
        ResourceBundle resources = ResourceBundle.getBundle("locale/" + portletName + "_" + response.getLocale().getLanguage(), request.getLocale(), utf8Control);
        Enumeration<String> key = resources.getKeys();

        while (key.hasMoreElements()) {
            String param = (String) key.nextElement();
            translations.put("tr." + param, resources.getString(param));
        }
        return translations;
    }

}
