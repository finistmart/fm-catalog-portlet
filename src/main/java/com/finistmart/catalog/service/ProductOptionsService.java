package com.finistmart.catalog.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.catalog.HttpClient;
import com.finistmart.catalog.portlet.cache.CacheFetcher;
import com.finistmart.catalog.portlet.cache.CacheHandler;
import com.finistmart.catalog.portlet.dto.ProductOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

@ApplicationScoped
public class ProductOptionsService {

    private static final Logger logger = LoggerFactory.getLogger(ProductOptionsService.class);

    public static String OPTIONS_CACHE_NAME = "options_byProductId";

    private final CacheHandler<String, List<ProductOption>> cacheHandler = new CacheHandler<>(new CacheFetcher<>());

    @Inject
    private JsonMapper jsonMapper;

    private ObjectMapper mapper() {
        return jsonMapper.mapper();
    }

    public void clearOptionsCache(String key) {
        this.cacheHandler.evict(OPTIONS_CACHE_NAME, key);
    }

    public List<ProductOption> fetchByProductId(HttpClient client, Long productId) {
        List<ProductOption> options = null;
        String requestUrl = "/api/productoptions";
        if (productId != null) {
            requestUrl += "?productId=" + String.valueOf(productId);
        }
        try {
            String responseBody = client.request(requestUrl, "GET");
            options = mapper().readValue(responseBody, new TypeReference<List<ProductOption>>() { });
        } catch (IOException e) {
            logger.error("Error saving product ", e);
        }

        return options;
    }

    public List<ProductOption> getByProductId(HttpClient client, Long productId) {
        List<ProductOption> options = cacheHandler.getEntityFromCache(OPTIONS_CACHE_NAME)
            .orFromSource(() -> {
                return this.fetchByProductId(client, productId);
            }).usingCacheKey(String.valueOf(productId))
        ;

        return options;
    }


}
