package com.finistmart.catalog.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.catalog.HttpClient;
import com.finistmart.catalog.portlet.cache.CacheFetcher;
import com.finistmart.catalog.portlet.cache.CacheHandler;
import com.finistmart.catalog.portlet.dto.InfoField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

@ApplicationScoped
public class InfoFieldsService {

    private static final Logger logger = LoggerFactory.getLogger(InfoFieldsService.class);

    public static String INFOFIELDS_CACHE_NAME = "infofields";
    public static String INFOFIELDS_CACHE_KEY = "all";

    private final CacheHandler<String, List<InfoField>> cacheHandler = new CacheHandler<>(new CacheFetcher<>());

    @Inject
    private JsonMapper jsonMapper;

    private ObjectMapper mapper() {
        return jsonMapper.mapper();
    }

    public List<InfoField> getInfoFields(HttpClient client) {
        List<InfoField> fields = cacheHandler.getEntityFromCache(INFOFIELDS_CACHE_NAME)
            .orFromSource(() -> {
                return this.fetchInfoFields(client);
            }).usingCacheKey(INFOFIELDS_CACHE_KEY)
        ;
        return fields;
    }

    public List<InfoField> fetchInfoFields(HttpClient client) {
        List<InfoField> infoFields = null;
        String requestUrl = "/api/infofields";


        try {
            String responseBody = client.request(requestUrl, "GET");
            infoFields = mapper().readValue(responseBody, new TypeReference<List<InfoField>>() { });
        } catch (IOException e) {
            logger.error("Error saving product ", e);
        }

        return infoFields;
    }

}
