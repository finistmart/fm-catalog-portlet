package com.finistmart.catalog.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.catalog.HttpClient;
import com.finistmart.catalog.portlet.cache.CacheFetcher;
import com.finistmart.catalog.portlet.cache.CacheHandler;
import com.finistmart.catalog.portlet.dto.Order;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class OrdersService {

    private static final Logger logger = LoggerFactory.getLogger(OrdersService.class);

    public static String ORDERS_CACHE_NAME = "orders";
    public static String ORDERS_CACHE_KEY = "all";

    private final CacheHandler<String, List<Order>> cacheHandler = new CacheHandler<>(new CacheFetcher<>());
    private final CacheHandler<String, Order> orderCacheHandler = new CacheHandler<>(new CacheFetcher<>());

    @Inject
    private JsonMapper jsonMapper;

    private ObjectMapper mapper() {
        return jsonMapper.mapper();
    }

    public void clearCache() {
        this.cacheHandler.clear(ORDERS_CACHE_NAME);
        this.orderCacheHandler.clear(ORDERS_CACHE_NAME);
    }

    public List<Order> fetchAll(HttpClient client, Long showCaseId) {

        List<Order> orders = new ArrayList<>();
        String requestUrl = "/api/orders";
        if (showCaseId != null) {
            requestUrl += "?showCaseId=" + String.valueOf(showCaseId);
        }
        try {
            String responseBody = client.request(requestUrl, "GET");
            orders = mapper().readValue(responseBody, new TypeReference<List<Order>>() { });
        } catch (IOException e) {
            logger.error("Error getting orders list", e);
        }

        return orders;
    }

    public List<Order> getAll(HttpClient client, Long showCaseId) {

        List<Order> orders = cacheHandler.getEntityFromCache(ORDERS_CACHE_NAME)
            .orFromSource(() -> {
                return this.fetchAll(client, showCaseId);
            }).usingCacheKey(showCaseId != null ? String.valueOf(showCaseId) : ORDERS_CACHE_KEY)
        ;

        return orders;
    }


    public Order getOrder(HttpClient client, Long orderId) {
        Order order = orderCacheHandler.getEntityFromCache(ORDERS_CACHE_NAME)
            .orFromSource(() -> {
                return this.fetchOrder(client, orderId);
            }).usingCacheKey(String.valueOf(orderId))
        ;

        return order;
    }

    public Order fetchOrder(HttpClient client, Long orderId) {
        Order order = null;
        String requestUrl = "/api/orders";
        if (orderId != null) {
            requestUrl += "/" + String.valueOf(orderId);
        }
        try {
            String responseBody = client.request(requestUrl, "GET");
            order = mapper().readValue(responseBody, new TypeReference<Order>() {});
        } catch (IOException e) {
            logger.error("Error getting product by id ", e);
        }

        return order;
    }

    public Order saveOrder(HttpClient client, Order order) {
        String requestUrl = "/api/orders/";
        if (order != null) {
            requestUrl += order.getId() + "/save";
        }

        try {
            String body = mapper().writeValueAsString(order);
            String responseBody = client.request(requestUrl, "PUT", body);
            order = mapper().readValue(responseBody, new TypeReference<Order>() { });
            cacheHandler.evict(ORDERS_CACHE_NAME, ORDERS_CACHE_KEY);
            orderCacheHandler.evict(ORDERS_CACHE_NAME, String.valueOf(order.getId()));
        } catch (IOException e) {
            logger.error("Error saving product ", e);
        }

        return order;
    }
}
