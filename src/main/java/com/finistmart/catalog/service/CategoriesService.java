package com.finistmart.catalog.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.catalog.HttpClient;
import com.finistmart.catalog.portlet.cache.CacheFetcher;
import com.finistmart.catalog.portlet.cache.CacheHandler;
import com.finistmart.catalog.portlet.dto.Category;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class CategoriesService {

    private static final Logger logger = LoggerFactory.getLogger(CategoriesService.class);

    public static String CATEGORIES_CACHE_NAME = "categories";
    public static String CATEGORIES_CACHE_KEY = "all";

    private final CacheHandler<String, List<Category>> cacheHandler = new CacheHandler<>(new CacheFetcher<>());

    @Inject
    private JsonMapper jsonMapper;

    private ObjectMapper mapper() {
        return jsonMapper.mapper();
    }

    public void clearCache() {
        this.cacheHandler.clear(CATEGORIES_CACHE_NAME);
    }

    public List<Category> fetchCategories(HttpClient client) {
        List<Category> categoryList = new ArrayList<>();
        String requestUrl = "/api/categories";
        try {
            String responseBody = client.request(requestUrl, "GET");
            categoryList = mapper().readValue(responseBody, new TypeReference<List<Category>>() { });
        } catch (IOException e) {
            logger.error("Error saving product ", e);
        }

        return categoryList;
    }


    public List<Category> getCategories(HttpClient client) {
        List<Category> categories = cacheHandler.getEntityFromCache(CATEGORIES_CACHE_NAME)
            .orFromSource(() -> {
                return this.fetchCategories(client);
            }).usingCacheKey(CATEGORIES_CACHE_KEY)
        ;
        return categories;
    }


    public Category addCategory(HttpClient client, String name, Long parentId) {

        String requestUrl = "/api/categories/add";

        if (parentId != null) {
            requestUrl += "?parentId=" + parentId;
        }

        Category category = new Category(null, name);

        try {
            String body = mapper().writeValueAsString(category);
            String responseBody = client.request(requestUrl, "POST", body);
            category = mapper().readValue(responseBody, new TypeReference<Category>() { });
            cacheHandler.evict(CATEGORIES_CACHE_NAME, CATEGORIES_CACHE_KEY);
        } catch (IOException e) {
            logger.error("Error adding category ", e);
        }

        return category;
    }

    public Category saveCategory(HttpClient client, Long categoryId, String name, Long parentId) {

        String requestUrl = "/api/categories/" + categoryId + "/save";

        if (parentId != null) {
            requestUrl += "?parentId=" + parentId;
        }

        Category category = new Category(categoryId, name);

        try {
            String body = mapper().writeValueAsString(category);
            String responseBody = client.request(requestUrl, "PUT", body);
            category = mapper().readValue(responseBody, new TypeReference<Category>() { });
            cacheHandler.evict(CATEGORIES_CACHE_NAME, CATEGORIES_CACHE_KEY);
        } catch (IOException e) {
            logger.error("Error saving category ", e);
        }

        return category;
    }

    public void deleteCategory(HttpClient client, Long categoryId) {

        String requestUrl = "/api/categories/" + categoryId + "/delete";

        try {
            String responseBody = client.request(requestUrl, "DELETE");
            cacheHandler.evict(CATEGORIES_CACHE_NAME, CATEGORIES_CACHE_KEY);
        } catch (IOException e) {
            logger.error("Error saving category ", e);
        }

    }
}
