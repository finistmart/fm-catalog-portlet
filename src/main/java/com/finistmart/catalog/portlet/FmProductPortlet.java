package com.finistmart.catalog.portlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.catalog.HttpClient;
import com.finistmart.catalog.portlet.dto.Category;
import com.finistmart.catalog.portlet.dto.IFieldValue;
import com.finistmart.catalog.portlet.dto.InfoField;
import com.finistmart.catalog.portlet.dto.OptionSetImage;
import com.finistmart.catalog.portlet.dto.Product;
import com.finistmart.catalog.portlet.dto.ProductFieldValue;
import com.finistmart.catalog.portlet.dto.ProductImage;
import com.finistmart.catalog.portlet.dto.ProductOption;
import com.finistmart.catalog.portlet.dto.ProductOptionSet;
import com.finistmart.catalog.portlet.dto.ProductOptionValue;
import com.finistmart.catalog.service.CategoriesService;
import com.finistmart.catalog.service.InfoFieldsService;
import com.finistmart.catalog.service.JsonMapper;
import com.finistmart.catalog.service.OptionSetsService;
import com.finistmart.catalog.service.ProductOptionsService;
import com.finistmart.catalog.service.ProductsService;
import com.finistmart.catalog.service.TranslationsService;
import com.finistmart.util.ParamUtil;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.annotations.ActionMethod;
import javax.portlet.annotations.PortletConfiguration;
import javax.portlet.annotations.Preference;
import javax.portlet.annotations.RenderMethod;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@PortletConfiguration(
        portletName = "FmProductPortlet",
        publicParams = {"productId", "optionSetId"},
        supportedLocales = {"en", "ru"},
        resourceBundle = "locale.ProductPortlet",
        prefs = {
            @Preference(name = "serviceUrl", values="http://localhost:8080/fm-service"),
            @Preference(name = "serviceLogin", values="finistmart"),
            @Preference(name = "servicePassword", values="finistmart"),
            @Preference(name = "currency", values="RUB")
        }
)
public class FmProductPortlet {
    private static final Logger logger = LoggerFactory.getLogger(FmProductPortlet.class);

    @Inject
    private PortletConfig pcfg;

    @Inject
    private ProductsService productsService;

    @Inject
    private CategoriesService categoriesService;

    @Inject
    private ProductOptionsService productOptionsService;

    @Inject
    private OptionSetsService optionSetsService;

    @Inject
    private InfoFieldsService infoFieldsService;

    @Inject
    private TranslationsService translationsService;

    @Inject
    private JsonMapper jsonMapper;

    private ObjectMapper mapper() {
        return jsonMapper.mapper();
    }

    HttpClient serviceClient(PortletRequest request) {
        PortletPreferences prefs = request.getPreferences();
        String serviceUrl = prefs.getValue("serviceUrl", null);
        String serviceLogin = prefs.getValue("serviceLogin", null);
        String servicePassword = prefs.getValue("servicePassword", null);

        HttpClient client = new HttpClient();
        client.setup(serviceUrl, serviceLogin, servicePassword);
        return client;
    }

    @ActionMethod(portletName = "FmProductPortlet", actionName = "saveSettings")
    public void saveSettings(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        FmCatalogApp.saveSettings(request, response);
    }

    @ActionMethod(portletName = "FmProductPortlet", actionName = "clearCache")
    public void clearCache(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        this.productsService.clearCache();
        this.categoriesService.clearCache();
    }

    @ActionMethod(portletName = "FmProductPortlet", actionName = "saveImage")
    public void saveImage(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        Long productId = ParamUtil.getLongParam("productId", null, request, response);
        Part part = null;
        try {
            part = request.getPart("image");
        } catch (Throwable t) {}
        InputStream is = part.getInputStream();
        if (productId != null) {
            Product product = productsService.getProduct(this.serviceClient(request), productId);
            ProductImage image = null;
            if (product.getImage() != null) {
                image = product.getImage();
            } else {
                image = new ProductImage();
            }
            image.setFileName(part.getSubmittedFileName());
            image.setMimeType(part.getContentType());
            String imageContents = Base64.getEncoder().encodeToString(IOUtils.toByteArray(is));
            image.setImageData(imageContents);
            product.setImage(image);
            productsService.saveProductImage(this.serviceClient(request), product);
        }
    }

    @ActionMethod(portletName = "FmProductPortlet", actionName = "saveOptionSetImage")
    public void saveOptionSetImage(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        Long optionSetId = ParamUtil.getLongParam("optionSetId", null, request, response);
        Part part = null;
        try {
            part = request.getPart("image");
        } catch (Throwable t) {
        }
        InputStream is = part.getInputStream();
        if (optionSetId != null) {
            ProductOptionSet optionSet = optionSetsService.getOptionSet(this.serviceClient(request), optionSetId);
            OptionSetImage image = new OptionSetImage();
            image.setFileName(part.getSubmittedFileName());
            image.setMimeType(part.getContentType());
            String imageContents = Base64.getEncoder().encodeToString(IOUtils.toByteArray(is));
            image.setImageData(imageContents);
            optionSet.setImage(image);

            optionSetsService.saveOptionSet(this.serviceClient(request), optionSet);
        }
    }

    @ActionMethod(portletName = "FmProductPortlet", actionName = "saveFieldValue")
    public void saveFieldValue(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        Long optionSetId = ParamUtil.getLongParam("optionSetId", null, request, response);
        Long productId = ParamUtil.getLongParam("productId", null, request, response);
        Long valueId = ParamUtil.getLongParam("valueId", null, request, response);

        String infoField = ParamUtil.getStringParam("infoField", null, request, response);
        String fieldValue = ParamUtil.getStringParam("fieldValue", null, request, response);

        Product product = productsService.getProduct(this.serviceClient(request), productId);
        if (optionSetId != null) {
            // save option set
            List<ProductOptionValue> optionValues = productsService.getOptionValues(this.serviceClient(request), optionSetId);
            ProductOptionValue optionValue = optionValues.stream()
                    .filter(opt -> valueId.equals(opt.getId())).findFirst().orElseGet(ProductOptionValue::new);
            productsService.saveOptionValue(this.serviceClient(request), optionValue, fieldValue);
        } else if (productId != null) {
            // save product field value
            productsService.saveFieldValue(this.serviceClient(request), product, infoField, fieldValue);
        }
    }

    @ActionMethod(portletName = "FmProductPortlet", actionName = "addFieldValue")
    public void addFieldValue(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        Long optionSetId = ParamUtil.getLongParam("optionSetId", null, request, response);
        Long productId = ParamUtil.getLongParam("productId", null, request, response);
        Long infoFieldId = ParamUtil.getLongParam("infoFieldId", null, request, response);
        String fieldValue = ParamUtil.getStringParam("fieldValue", null, request, response);
        if (optionSetId != null) {
            // save option set
            productsService.addOptionValue(this.serviceClient(request), optionSetId, infoFieldId, fieldValue);
        } else if (productId != null) {
            // save product field value
            productsService.addFieldValue(this.serviceClient(request), productId, infoFieldId, fieldValue);
        }
    }

    @ActionMethod(portletName = "FmProductPortlet", actionName = "delFieldValue")
    public void delFieldValue(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        Long optionSetId = ParamUtil.getLongParam("optionSetId", null, request, response);
        Long valueId = ParamUtil.getLongParam("valueId", null, request, response);
        Long infoFieldId = ParamUtil.getLongParam("infoFieldId", null, request, response);
        if (optionSetId != null) {
            List<ProductOptionValue> optionValues = productsService.getOptionValues(this.serviceClient(request), optionSetId);
            ProductOptionValue optionValue = infoFieldId != null ? optionValues.stream()
                    .filter(opt -> valueId.equals(valueId) && infoFieldId.equals(opt.getInfoField().getId())).findFirst().orElseGet(ProductOptionValue::new): null;
            if (optionValue.getId() != null) {
                // save option set
                productsService.delOptionValue(this.serviceClient(request), valueId);
            } else {
                productsService.delFieldValue(this.serviceClient(request), valueId);
            }
        } else {
            // save product field value
            productsService.delFieldValue(this.serviceClient(request), valueId);
        }
    }

    @ActionMethod(portletName = "FmProductPortlet", actionName = "saveName")
    public void saveName(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        Long productId = ParamUtil.getLongParam("productId", null, request, response);
        String name = ParamUtil.getStringParam("name", null, request, response);
        Long optionSetId = ParamUtil.getLongParam("optionSetId", null, request, response);
        if (productId != null) {
            if (optionSetId != null) {
                ProductOptionSet optionSet = optionSetsService.getOptionSet(this.serviceClient(request), optionSetId);
                optionSet.setName(name);
                optionSetsService.saveOptionSet(this.serviceClient(request), optionSet);
            } else {
                Product product = productsService.getProduct(this.serviceClient(request), productId);
                product.setName(name);
                productsService.saveProduct(this.serviceClient(request), product);
            }
        }
    }

    @ActionMethod(portletName = "FmProductPortlet", actionName = "addOptionSet")
    public void addOptionSet(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        Long productId = ParamUtil.getLongParam("productId", null, request, response);
        Long optionId = ParamUtil.getLongParam("optionId", null, request, response);
        String name = ParamUtil.getStringParam("name", null, request, response);
        if (productId != null) {
            ProductOptionSet optionSet = optionSetsService.addOptionSet(this.serviceClient(request), productId, optionId, name);
        }
    }

    @ActionMethod(portletName = "FmProductPortlet", actionName = "deleteOptionSetOrProduct")
    public void deleteOptionSet(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        Long productId = ParamUtil.getLongParam("productId", null, request, response);
        Long optionSetId = ParamUtil.getLongParam("optionSetId", null, request, response);
        if (productId != null) {
            Product product = productsService.getProduct(this.serviceClient(request), productId);
            if (optionSetId != null) {
                ProductOptionSet optionSet = optionSetsService.getOptionSet(this.serviceClient(request), optionSetId);
                optionSet.setProduct(product);
                optionSetsService.deleteOptionSet(this.serviceClient(request), optionSet);
            } else {
                productsService.deleteProduct(this.serviceClient(request), product);
            }
        }
    }

    @ActionMethod(portletName = "FmProductPortlet", actionName = "addProduct")
    public void addProduct(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        Long categoryId = ParamUtil.getLongParam("categoryId", null, request, response);
        String productName = ParamUtil.getStringParam("productName", null, request, response);
        if (categoryId != null) {
            Product product = new Product();
            product.setName(productName);
            productsService.addProduct(this.serviceClient(request), product, categoryId);
        }
    }

    @ActionMethod(portletName = "FmProductPortlet", actionName = "setCategory")
    public void setCategory(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        Long categoryId = ParamUtil.getLongParam("categoryId", null, request, response);
        Long productId = ParamUtil.getLongParam("productId", null, request, response);

        if (productId != null && categoryId != null) {
            Product product = productsService.getProduct(this.serviceClient(request), productId);
            List<Category> categories = categoriesService.getCategories(this.serviceClient(request));
            categories = categories.stream().filter((cat) -> cat.getId().equals(categoryId)).collect(Collectors.toList());
            if (categories.size() > 0) {
                productsService.setCategory(this.serviceClient(request), product, categories.get(0));
            }
        }
    }

   @RenderMethod(portletNames = "FmProductPortlet", ordinal = 100, include = "/WEB-INF/jsp/productView.jsp")
   public void render(RenderRequest req, RenderResponse resp) throws PortletException {
       Map<String, String> translations = translationsService.getTranslationsMap(req, resp, "ProductPortlet");

       PortletPreferences prefs = req.getPreferences();

       List<String> names = Collections.list(prefs.getNames());
       for (String name : names) {
           req.setAttribute(name, prefs.getValue(name, ""));
       }

       req.setAttribute("settings", names);

       Long productId = ParamUtil.getLongParam("productId", null, req, resp);
       Product product = productsService.getProduct(this.serviceClient(req), productId);
       if (product == null) {
           throw new PortletException("Product not found");
       }
       Long optionSetId = ParamUtil.getLongParam("optionSetId", null, req, resp);

       final Map<String, ProductOptionValue> optionValueMap = new HashMap<>();
       List<InfoField> infoFields = infoFieldsService.getInfoFields(this.serviceClient(req));
       List<ProductFieldValue> productFieldValues = product.getFieldValues();
       Map<String, ProductFieldValue> fieldValuesMap = new HashMap<>();
       for (ProductFieldValue fieldValue : productFieldValues) {
           if (fieldValue.getInfoField() != null && fieldValue.getInfoField().getName() != null) {
               fieldValuesMap.put(fieldValue.getInfoField().getName(), fieldValue);
           }
       }
       List<InfoField> unsetInfoFields = new ArrayList<>();
       unsetInfoFields = infoFields.stream()
               .filter((field) -> {
                   if (optionSetId != null) {
                       return true;
                   } else {
                       return !fieldValuesMap.containsKey(field.getName());
                   }
               }).collect(Collectors.toList());
       String productTitle = product.getName();
       List<ProductOption> productOptions = productOptionsService.getByProductId(this.serviceClient(req), product.getId());
       if (optionSetId != null) {
           ProductOptionSet optionSet = optionSetsService.getOptionSet(this.serviceClient(req), optionSetId);
           if (optionSet != null && optionSet.getId() != null) {
               productTitle = optionSet.getName();

               List<ProductOptionValue> optionValues = productsService.getOptionValues(this.serviceClient(req), optionSetId);

               for (ProductOptionValue optionValue : optionValues) {
                   optionValueMap.put(optionValue.getInfoField().getName(), optionValue);
               }
               unsetInfoFields = unsetInfoFields.stream()
                       .filter((field) -> (!optionValueMap.containsKey(field.getName()))).collect(Collectors.toList());
           }
       }

       Map<String, IFieldValue> totalFieldValueMap = new HashMap<>(fieldValuesMap);
       if (optionSetId != null) {
           totalFieldValueMap.putAll(optionValueMap);
       }
       List<Map.Entry<String, IFieldValue>> entries = new ArrayList<>(totalFieldValueMap.entrySet());
       Collections.sort(entries, (Comparator<Map.Entry<String, IFieldValue>>) (e1, e2) -> e1.getValue().getInfoField().getWeight() - e2.getValue().getInfoField().getWeight());
       LinkedHashMap<String, IFieldValue> sortedValues = new LinkedHashMap<String, IFieldValue>(entries.size());
       for(Map.Entry<String, IFieldValue> entry : entries){ sortedValues.put(entry.getKey(), entry.getValue()); }

       String currency = prefs.getValue("currency", "");
       IFieldValue productPriceValue = sortedValues.get("price");
       if (sortedValues.get("currency") != null) {
           currency = sortedValues.get("currency").getValue();
       }
       String priceValue = productPriceValue != null ? productPriceValue.getValue() : "0";

       List<Category> categories = categoriesService.getCategories(this.serviceClient(req));

       req.setAttribute("categories", categories);
       req.setAttribute("priceValue", priceValue);
       req.setAttribute("currency", currency);
       req.setAttribute("optionValues", optionValueMap);
       req.setAttribute("totalFieldValueMap", sortedValues);
       req.setAttribute("unsetInfoFields", unsetInfoFields);
       req.setAttribute("optionSetId", optionSetId);
       req.setAttribute("product", product);
       req.setAttribute("productTitle", productTitle);
       req.setAttribute("productOptions", productOptions);
       try {
           req.setAttribute("productJson", mapper().writeValueAsString(product));
           req.setAttribute("settingsJson", mapper().writeValueAsString(names));
           req.setAttribute("l11nJson", mapper().writeValueAsString(translations));
       } catch (JsonProcessingException e) {
           logger.error("Error generating json for view", e);
       }
   }




}
