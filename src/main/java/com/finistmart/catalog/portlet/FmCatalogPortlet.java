package com.finistmart.catalog.portlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.catalog.HttpClient;
import com.finistmart.catalog.portlet.dto.ProductsResult;
import com.finistmart.catalog.service.JsonMapper;
import com.finistmart.catalog.service.ProductsService;
import com.finistmart.catalog.service.TranslationsService;
import com.finistmart.util.ParamUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.annotations.ActionMethod;
import javax.portlet.annotations.PortletConfiguration;
import javax.portlet.annotations.Preference;
import javax.portlet.annotations.RenderMethod;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;


@PortletConfiguration(portletName="FmCatalogPortlet", publicParams = {"categoryId", "pageNum"},
        supportedLocales = {"en", "ru"},
        resourceBundle = "locale.CatalogPortlet",
        prefs = {
            @Preference(name = "serviceUrl", values="http://localhost:8080/fm-service"),
            @Preference(name = "serviceLogin", values="finistmart"),
            @Preference(name = "servicePassword", values="finistmart"),
            @Preference(name = "productLinkTplStr", values="<a href='http://localhost:8080/pluto/portal/product?productId={{ productId }}'></a>"),
            @Preference(name = "pageSize", values="12")
        }
)
public class FmCatalogPortlet {

    private static final Logger logger = LoggerFactory.getLogger(FmCatalogPortlet.class);

    @Inject
    private PortletConfig pcfg;

    @Inject
    private ProductsService productsService;

    @Inject
    private TranslationsService translationsService;

    @Inject
    private JsonMapper jsonMapper;

    private ObjectMapper mapper() {
        return jsonMapper.mapper();
    }

    HttpClient serviceClient(PortletRequest request) {
        PortletPreferences prefs = request.getPreferences();
        String serviceUrl = prefs.getValue("serviceUrl", null);
        String serviceLogin = prefs.getValue("serviceLogin", null);
        String servicePassword = prefs.getValue("servicePassword", null);

        HttpClient client = new HttpClient();
        client.setup(serviceUrl, serviceLogin, servicePassword);
        return client;
    }

    @ActionMethod(portletName = "FmCatalogPortlet", actionName = "saveSettings")
    public void saveSettings(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        FmCatalogApp.saveSettings(request, response);
    }

    @ActionMethod(portletName = "FmCatalogPortlet", actionName = "clearCache")
    public void clearCache(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        this.productsService.clearCache();
    }

    @RenderMethod(portletNames = "FmCatalogPortlet", ordinal = 100, include = "/WEB-INF/jsp/catalogView.jsp")
    public void render(RenderRequest req, RenderResponse resp) {
        Map<String, String> translations = translationsService.getTranslationsMap(req, resp, "CatalogPortlet");
        PortletPreferences prefs = req.getPreferences();

        List<String> names = Collections.list(prefs.getNames());
        for (String name : names) {
            req.setAttribute(name, prefs.getValue(name, ""));
        }
        req.setAttribute("settings", names);

        Long categoryId = ParamUtil.getLongParam("categoryId", null, req, resp);

        Long pageNumParam = ParamUtil.getLongParam("pageNum", 1L, req, resp);
        Integer pageNum = pageNumParam.intValue();
        String pageSizeParam = prefs.getValue("pageSize", "12");
        Integer pageSize = Integer.valueOf(pageSizeParam);

        ProductsResult result = productsService.getProducts(this.serviceClient(req), categoryId, pageNum, pageSize);

        try {
            if (result != null) {
                long totalPages = (result.getTotal() + result.getPageSize() - 1) / result.getPageSize();
                req.setAttribute("productsJson", mapper().writeValueAsString(result.getResults()));
                req.setAttribute("pageSize", mapper().writeValueAsString(result.getPageSize()));
                req.setAttribute("total", mapper().writeValueAsString(result.getTotal()));
                req.setAttribute("totalPages", mapper().writeValueAsString(totalPages));
                req.setAttribute("pageNum", mapper().writeValueAsString(result.getPage()));
            }
            req.setAttribute("settingsJson", mapper().writeValueAsString(names));
            req.setAttribute("l11nJson", mapper().writeValueAsString(translations));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}
