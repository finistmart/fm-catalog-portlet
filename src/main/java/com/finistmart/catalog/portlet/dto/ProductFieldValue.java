package com.finistmart.catalog.portlet.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;


public class ProductFieldValue implements IFieldValue, Serializable {

    private Long id;

    private InfoField infoField;

    @JsonIgnore
    private Product product;

    private String value;


    public ProductFieldValue() {
    }


    // getters & setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InfoField getInfoField() {
        return infoField;
    }

    public void setInfoField(InfoField infoField) {
        this.infoField = infoField;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
