package com.finistmart.catalog.portlet.dto;


import java.io.Serializable;


public class OptionValueAdd implements Serializable {

    private Long optionSetId;

    private Long infoFieldId;

    private String value;

    public OptionValueAdd() {
    }

    public OptionValueAdd(Long optionSetId, Long infoFieldId, String value) {
        this.optionSetId = optionSetId;
        this.infoFieldId = infoFieldId;
        this.value = value;
    }
// getters & setters

    public Long getOptionSetId() {
        return optionSetId;
    }

    public void setOptionSetId(Long optionSetId) {
        this.optionSetId = optionSetId;
    }

    public Long getInfoFieldId() {
        return infoFieldId;
    }

    public void setInfoFieldId(Long infoFieldId) {
        this.infoFieldId = infoFieldId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
