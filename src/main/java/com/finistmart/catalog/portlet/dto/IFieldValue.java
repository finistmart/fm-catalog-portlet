package com.finistmart.catalog.portlet.dto;

public interface IFieldValue {

    // getters & setters
    Long getId();

    void setId(Long id);

    InfoField getInfoField();

    void setInfoField(InfoField infoField);

    String getValue();

    void setValue(String value);
}
