package com.finistmart.catalog.portlet.dto;


import java.io.Serializable;


public class ProductOptionSet implements Serializable {

    private Long id;

    private String name;

    private ProductOption productOption;

    private Product product;

    private OptionSetImage image;

    public ProductOptionSet() {
    }

    public ProductOptionSet(String name, ProductOption productOption, Product product) {
        this.name = name;
        this.productOption = productOption;
        this.product = product;
    }

    public ProductOptionSet(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    // getters & setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductOption getProductOption() {
        return productOption;
    }

    public void setProductOption(ProductOption productOption) {
        this.productOption = productOption;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public OptionSetImage getImage() {
        return image;
    }

    public void setImage(OptionSetImage image) {
        this.image = image;
    }
}
