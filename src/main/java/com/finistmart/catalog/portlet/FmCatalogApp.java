package com.finistmart.catalog.portlet;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.catalog.service.JsonMapper;
import com.finistmart.util.ParamUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.HeaderRequest;
import javax.portlet.HeaderResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.ReadOnlyException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;
import javax.portlet.annotations.HeaderMethod;
import javax.portlet.annotations.PortletApplication;
import javax.portlet.annotations.PortletQName;
import javax.portlet.annotations.PublicRenderParameterDefinition;
import javax.portlet.annotations.ServeResourceMethod;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@PortletApplication(
    publicParams = {
        @PublicRenderParameterDefinition(identifier = "productId", qname = @PortletQName(localPart = "productId", namespaceURI = "")),
        @PublicRenderParameterDefinition(identifier = "optionSetId", qname = @PortletQName(localPart = "optionSetId", namespaceURI = ""))
})
public class FmCatalogApp {

    private static final Logger logger = LoggerFactory.getLogger(FmCatalogApp.class);

    @Inject
    private JsonMapper jsonMapper;

    private ObjectMapper mapper() {
        return jsonMapper.mapper();
    }

    @ServeResourceMethod(portletNames = {"FmProductPortlet", "FmCatalogPortlet", "FmOrdersPortlet", "FmCategoriesPortlet"},
            resourceID = "getSettings", contentType = "application/json")
    public void getSettings(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {

        response.setContentType("application/json");
        PortletPreferences prefs = request.getPreferences();

        Map<String, Object> settingsResponse = new HashMap<String, Object>();

        List<String> names = Collections.list(prefs.getNames());
        for (String name : names) {
            settingsResponse.put(name, prefs.getValue(name, ""));
        }

        PrintWriter out = response.getWriter();

        out.write(mapper().writeValueAsString(settingsResponse));
        out.flush();
        out.close();

    }

    public static void saveSettings(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        PortletPreferences prefs = request.getPreferences();

        try {
            List<String> names = Collections.list(prefs.getNames());
            for (String name : names) {
                String param = extParam(ParamUtil.getStringParam(name, null, request, response));
                if (param != null) {
                    prefs.setValue(name, param);
                }
            }
            prefs.store();
        } catch (ReadOnlyException e) {
            logger.error("Error saving readonly portlet preference", e);
        } catch (IOException e) {
            logger.error("Error reading/writing portlet preference", e);
        } catch (ValidatorException e) {
            logger.error("Error validating portlet preference", e);
        }

    }

    public static String extParam(String value) throws UnsupportedEncodingException {
        return value != null ? URLDecoder.decode(new String(Base64.getDecoder().decode(value)), "UTF-8") : null;
    }

    @HeaderMethod(portletNames = {"FmCatalogPortlet", "FmProductPortlet", "FmCategoriesPortlet", "FmOrdersPortlet"})
    public void renderHeaders(HeaderRequest headerRequest, HeaderResponse headerResponse) throws PortletException,
            IOException {

        headerResponse.addDependency("load-sk-tpl-bundle.js", "com.skinny-widgets", "1.1.50",
                "<script>" +
                        "const loadTemplates = async () => {\n" +
                        "    const response = await fetch('" + headerRequest.getContextPath() + "/fm-catalog/node_modules/skinny-widgets/dist/antd.templates.html')\n" +
                        "        .then(response => response.text())\n" +
                        "        .then(text => document.head.insertAdjacentHTML('beforeend', text));\n" +
                        "};\n" +
                        "loadTemplates();" +
                        "</script>"
        );
        headerResponse.addDependency("jsonpath.js", "com.complets", "0.0.2",
                "<script src=\"" + headerRequest.getContextPath() + "/fm-catalog/node_modules/complets/jsonpath.js\"></script>");
        headerResponse.addDependency("dialog-polyfill.js", "com.skinny-widgets", "1.1.50",
                "<script src=\"" + headerRequest.getContextPath() + "/fm-catalog/node_modules/skinny-widgets/compat/dialog-polyfill.js\"></script>");
        headerResponse.addDependency("sk-compat-bundle.js", "com.skinny-widgets", "1.1.50",
                "<script src=\"" + headerRequest.getContextPath() + "/fm-catalog/node_modules/skinny-widgets/dist/sk-compat-bundle.js\"></script>");
        headerResponse.addDependency("fm-catalog-portlet-bundle.js", "com.finistmart", "0.0.2",
                "<script src=\"" + headerRequest.getContextPath() + "/fm-catalog/fm-catalog-portlet-bundle.js\"></script>");
    }


}
