package com.finistmart.catalog.portlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finistmart.catalog.HttpClient;
import com.finistmart.catalog.portlet.dto.Category;
import com.finistmart.catalog.portlet.dto.Product;
import com.finistmart.catalog.service.CategoriesService;

import com.finistmart.catalog.service.JsonMapper;
import com.finistmart.catalog.service.ProductsService;
import com.finistmart.catalog.service.TranslationsService;
import com.finistmart.util.ParamUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.annotations.ActionMethod;
import javax.portlet.annotations.PortletConfiguration;
import javax.portlet.annotations.Preference;
import javax.portlet.annotations.RenderMethod;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@PortletConfiguration(portletName="FmCategoriesPortlet", publicParams = {"categoryId"},
    supportedLocales = {"en", "ru"},
    resourceBundle = "locale.CategoriesPortlet",
    prefs = {
        @Preference(name = "serviceUrl", values="http://localhost:8080/fm-service"),
        @Preference(name = "serviceLogin", values="finistmart"),
        @Preference(name = "servicePassword", values="finistmart"),
        @Preference(name = "categoryLinkTplStr", values="<a href='/?categoryId={{ id }}'>{{ name }}</a>")
    }
)
public class FmCategoriesPortlet {

    private static final Logger logger = LoggerFactory.getLogger(FmCategoriesPortlet.class);

    @Inject
    private PortletConfig pcfg;

    @Inject
    private CategoriesService categoriesService;

    @Inject
    private ProductsService productsService;

    @Inject
    private TranslationsService translationsService;

    @Inject
    private JsonMapper jsonMapper;

    private ObjectMapper mapper() {
        return jsonMapper.mapper();
    }

    HttpClient serviceClient(PortletRequest request) {
        PortletPreferences prefs = request.getPreferences();

        String serviceUrl = prefs.getValue("serviceUrl", null);
        String serviceLogin = prefs.getValue("serviceLogin", null);
        String servicePassword = prefs.getValue("servicePassword", null);

        HttpClient client = new HttpClient();
        client.setup(serviceUrl, serviceLogin, servicePassword);
        return client;
    }

    @ActionMethod(portletName = "FmCategoriesPortlet", actionName = "saveSettings")
    public void saveSettings(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        FmCatalogApp.saveSettings(request, response);
    }

    @ActionMethod(portletName = "FmCategoriesPortlet", actionName = "clearCache")
    public void clearCache(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        this.categoriesService.clearCache();
    }

    @RenderMethod(portletNames = "FmCategoriesPortlet", ordinal = 100, include = "/WEB-INF/jsp/categoriesView.jsp")
    public void render(RenderRequest req, RenderResponse resp) {
        Map<String, String> translations = translationsService.getTranslationsMap(req, resp, "CategoriesPortlet");
        PortletPreferences prefs = req.getPreferences();

        List<String> names = Collections.list(prefs.getNames());
        for (String name : names) {
            req.setAttribute(name, prefs.getValue(name, ""));
        }

        req.setAttribute("settings", names);

        List<Category> categories = categoriesService.getCategories(this.serviceClient(req));
        req.setAttribute("categories", categories);

        Long selectedCategoryId = ParamUtil.getLongParam("categoryId", null, req, resp);
        if (selectedCategoryId == null) {
            Long productId = ParamUtil.getLongParam("productId", null, req, resp);
            if (productId != null) {
                Product product = productsService.getProduct(this.serviceClient(req), productId);
                if (product != null && product.getCategories() != null && product.getCategories().size() > 0) {
                    selectedCategoryId = product.getCategories().get(0).getId();
                }
            }
        }
        req.setAttribute("selectedCategoryId", selectedCategoryId);
        try {
            req.setAttribute("categoriesJson", mapper().writeValueAsString(categories));
            req.setAttribute("settingsJson", mapper().writeValueAsString(names));
            req.setAttribute("l11nJson", mapper().writeValueAsString(translations));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @ActionMethod(portletName = "FmCategoriesPortlet", actionName = "addCategory")
    public void addCategory(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        String name = ParamUtil.getStringParam("name", null, request, response);
        Long parentCategoryId = ParamUtil.getLongParam("parentCategoryId", null, request, response);

        categoriesService.addCategory(this.serviceClient(request), name, parentCategoryId);
    }

    @ActionMethod(portletName = "FmCategoriesPortlet", actionName = "saveCategory")
    public void saveCategory(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        Long categoryId = ParamUtil.getLongParam("editCategoryId", null, request, response);
        String name = ParamUtil.getStringParam("name", null, request, response);
        Long parentCategoryId = ParamUtil.getLongParam("parentCategoryId", null, request, response);

        categoriesService.saveCategory(this.serviceClient(request), categoryId, name, parentCategoryId);
    }

    @ActionMethod(portletName = "FmCategoriesPortlet", actionName = "deleteCategory")
    public void deleteCategory(ActionRequest request, ActionResponse response) throws PortletException, IOException {
        Long categoryId = ParamUtil.getLongParam("categoryId", null, request, response);

        categoriesService.deleteCategory(this.serviceClient(request), categoryId);
    }
}
