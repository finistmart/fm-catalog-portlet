package com.finistmart.util;


import javax.portlet.ActionParameters;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.RenderURL;
import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestWrapper;
import java.util.Objects;

public class ParamUtil {

    public static String getStringParam(String name, String defaultValue, RenderRequest req, RenderResponse resp) {
        RenderURL renderURL = resp.createRenderURL();
        MutableRenderParameters renderParams = renderURL.getRenderParameters();
        String idString = renderParams.getValue(name);
        if (idString == null) {
            ServletRequestWrapper wrapper = (ServletRequestWrapper) req.getAttribute("com.liferay.portal.kernel.servlet.PortletServletRequest");
            if (wrapper != null) {
                ServletRequest servletRequest = ((ServletRequestWrapper) (wrapper.getRequest())).getRequest();
                idString = servletRequest.getParameter(name);
            }
        }
        return idString != null ? idString : defaultValue;
    }
    public static String getStringParam(String name, String defaultValue, ActionRequest req, ActionResponse resp) {
        ActionParameters params = req.getActionParameters();
        String idString = params.getValue(name);
        if (idString == null) {
            ServletRequestWrapper wrapper = (ServletRequestWrapper) req.getAttribute("com.liferay.portal.kernel.servlet.PortletServletRequest");
            if (wrapper != null) {
                ServletRequest servletRequest = ((ServletRequestWrapper) (wrapper.getRequest())).getRequest();
                idString = servletRequest.getParameter(name);
            }
        }
        return idString != null ? idString : defaultValue;
    }
    public static Long getLongParam(String name, Long defaultValue, RenderRequest req, RenderResponse resp) {
        String idString = ParamUtil.getStringParam(name, defaultValue != null ? String.valueOf(defaultValue) : null, req, resp);
        if (idString != null && !Objects.equals(idString, "") && !Objects.equals(idString, "null")) {
            return Long.parseLong(idString);
        } else {
            if (defaultValue != null) {
                return defaultValue;
            }
        }
        return null;
    }
    public static Long getLongParam(String name, Long defaultValue, ActionRequest req, ActionResponse resp) {
        String idString = ParamUtil.getStringParam(name, defaultValue != null ? String.valueOf(defaultValue) : null, req, resp);
        if (idString != null && !Objects.equals(idString, "") && !Objects.equals(idString, "null")) {
            return Long.parseLong(idString);
        } else {
            if (defaultValue != null) {
                return defaultValue;
            }
        }
        return null;
    }
}
